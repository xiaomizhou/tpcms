<?php
/** [评论视图模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-16 15:19:15
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:48:41
 */
namespace Common\Model;
use Think\Model\ViewModel;
class UserCommentViewModel extends ViewModel{

	public $tablename = 'user_comment';

	public $viewFields = array(

		'user_comment'=>array(
			'*',
			'_type'=>'INNER',
		),
		'user'=>array(
			'username','uid',
			'_type'=>'INNER',
			'_on' =>'user.uid=user_comment.user_uid',
		),
		'article'=>array(
			'article_title',
			'_type'=>'INNER',
			'_on' =>'article.aid=user_comment.article_aid',
		),
		
	); 
}