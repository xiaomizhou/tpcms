<?php
$db = require './Data/Config/db.inc.php'; 			   //数据库配置
$web = require './Data/Config/config.inc.php';	   	   //网站配置信息
$flag = include "Data/Config/flag.inc.php";			   //文档属性
$style = include "Data/Config/theme.inc.php";		   //网站风格
$config = array(
	//'配置项'=>'配置值'
	// 模块设置
	'MODULE_ALLOW_LIST'=>array('Admin','Home','Member'),
	'DEFUALT_MODULE'=>'Home',

	/***验证码设置***/ 
	'CODE_FONT'				=> 'Data/Font/font.ttf',    //字体
	'CODE_STR' 				=> '0123456789', 			//验证码种子
	'CODE_WIDTH' 			=> 80,         				//宽度
	'CODE_HEIGHT' 			=> 32,         				//高度
	'CODE_BG_COLOR' 		=> '#ffffff',   			//背景颜色
	'CODE_LEN' 				=> 4,           			//文字数量
	'CODE_FONT_SIZE' 		=> 16,          			//字体大小
	'CODE_FONT_COLOR'		=> '',   					//字体颜色


	/***auth验证***/
	'auth_superadmin'       =>array(1),
	'auth_action_name'	    =>array('index','edit','add','del','beachdelete','rule'),

	/***其他设置***/
	'TMPL_STRIP_SPACE'		=>false,  //是否去除模板文件里面的html空格与换行
	'TAGLIB_BUILD_IN'		=>'Cx,Hd',//自定义标签	
	'TMPL_ACTION_ERROR' 	=> './Data/Public/error.html',//默认错误跳转对应的模板文件
	'TMPL_ACTION_SUCCESS'	=> './Data/Public/success.html',//默认成功跳转对应的模板文件
	'TMPL_EXCEPTION_FILE'	=>'./Data/Public/404.html',// 异常页面的模板文件

	'LANG_SWITCH_ON' => true,   // 开启语言包功能
	'LANG_AUTO_DETECT' => false, // 自动侦测语言 开启多语言功能后有效
	'LANG_LIST'        => 'zh-cn,en-us', // 允许切换的语言列表 用逗号分隔
	'VAR_LANGUAGE'     => 'l', // 默认语言切换变量
	
	'URL_CASE_INSENSITIVE'  => true,//地址栏url大小写忽略
	//'show_page_trace'	    => true,

	'VAR_SESSION_ID' 				=> 'SSID',					//session
);
return array_merge($db,$web,$flag,$style,$config);