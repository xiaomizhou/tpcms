<?php
/**[模型表模型]
 * @Author: happy
 * @Email:  976123967@qq.com
 * @Date:   2015-03-15 21:20:53
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-02 00:08:24
 */
namespace Common\Service;
use Think\Model;
class ModelService extends Model{

	private $cache;

	public function _initialize()
	{
		$this->cache = S('model');
	}
	

	/**
	 * [get_all 所有模型]
	 * @return [type] [description]
	 */
	public function get_all()
	{
		return $this->cache;
	}

	/**
	 * [get_one 单条模型]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function get_one($id)
	{
		return isset($this->cache[$id])?$this->cache[$id]:null;
	}

	
}