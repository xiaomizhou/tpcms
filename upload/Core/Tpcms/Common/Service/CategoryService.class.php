<?php
/** [栏目模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-17 16:39:45
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-04 14:42:45
 */
namespace Common\Service;
use Think\Model;
use Third\Data;
class CategoryService extends Model{


	private $cache;

	public function _initialize()
	{
		$this->cache = S('category');
	}



	/**
	 * [get_page_title 网站标题]
	 * @param  [type] $cid [description]
	 * @return [type]      [description]
	 */
	public function get_page_title($cid)
	{
		$result = array('page_title'=>'','page_title_en'=>'');
		if(!$cid)
			return $result;
		$parents = Data::parentChannel($this->cache,$cid);
		
		if($parents)
		{
			foreach($parents as $v)
			{
				$result['page_title'] .= $v['cname']." > ";
				$result['page_title_en'] .= $v['cname_en']." > ";
			}
			$result['page_title'] .=C('cfg_name');
			$result['page_title_en'] .=C('cfg_name_en');

		}

		return $result;
	}


	/**
	 * [get_page_bread 面包导航]
	 * @param  [type] $cid [description]
	 * @return [type]      [description]
	 */
	public function get_page_bread($cid)
	{

		$result = array('page_title'=>'','page_title_en'=>'');
		if(!$cid)
			return $result;
		import('ORG.Util.Data');
		$parents = Data::parentChannel($this->cache,$cid);
		$parents = array_reverse($parents);
		$bread = '<a href="'.__ROOT__.'/">首页</a> &gt;';
		$breadEn = '<a href="'.__ROOT__.'/">Home</a> &gt;';
		if($parents)
		{
			foreach($parents as $v)
			{
				$remark =  strtolower($v['remark']);
				$url = $this->get_url($v);
				$bread .= '<a href='.$url[0].'>'.$v['cname'].'</a> &gt; ';
				$breadEn .= '<a href='.$url.'>'.$v['cname_en'].'</a> &gt; ';
				
			}

			$bread = rtrim($bread,'&gt; ');
			$breadEn = rtrim($breadEn,'&gt; ');
		}

		return array('bread'=>$bread,'bread_en'=>$breadEn);
	}




	/**
	 * [get_nav 导航]
	 * @param  integer $pid [description]
	 * @return [type]       [description]
	 */
	public function get_nav($pid=0)
	{
		
		$nav = array();
		// 没有栏目
		if(!$this->cache) 
			return $nav;
		// 当前浏览的cid的所有父级cid
		$parentCids = array();
		$cid = I('get.cid',null,'intval');
		if(!$cid)
		{
			$search = I('get.s');
			$search = explode('-', $search);
			$cid = array_shift($search);
		}
		if($cid)
		{
			$parentCids = D('Category')->get_parent_cid($cid);
		}
		// 遍历栏目组合数组
	
		foreach($this->cache as $k=> $v)
		{
			$nav[$k] = $v;
			$url = $this->get_url($v);
			$nav[$k]['url']= $url[0];
			$nav[$k]['murl']= $url[1];
			// 判断高亮
			if(in_array($v['cid'], $parentCids))
				$nav[$k]['cur'] = 1;
			else
				$nav[$k]['cur'] = 0;
		
		}
		return  Data::channelLevel($nav,$pid);
	}
	/**
	 * [get_url 栏目链接]
	 * @param  [type] $cat [description]
	 * @return [type]       [description]
	 */
	public function get_url($cat)
	{
		// 根据类型生成url
		$temp = array('cat_type'=>$cat['cat_type'],'cid'=>$cat['cid'],'go_url'=>$cat['go_url']);

		$child = D('Category')->get_child_list($cat['cid']);
		// 有设置跳转到子栏目 并且有子栏目
		if($cat['go_child']&&$child)
		{

			// 第一个子栏目
			$first = array_shift($child);
			$temp['cat_type']= $first['cat_type'];
			$temp['cid']= $first['cid'];
			$temp['go_url']= $first['go_url'];
		}

		$remark = strtolower($cat['remark']);
		switch ($temp['cat_type']) {
			case 1: //普通栏目
				$url  =  U('/'.$remark."_l_".$temp['cid']);
				$murl =  U('Mobile/'.$cat['remark'].'/lists',$temp);
				break;
			case 2: //封面栏目
				$url =  U('/'.$remark."_c_".$temp['cid']);
				$murl =  U('Mobile/'.$cat['remark'].'/cover',$temp);
				break;
			case 3: //跳转
				$url = $temp['go_url'];
				$murl= $temp['go_url'];
				break;
			case 4:
				$url  =  U('/'.$remark.'_v_'.$temp['cid']);
				$murl =  U('Mobile/'.$cat['remark'].'/view',$temp);
				break;	
		}

		/*switch ($temp['cat_type']) {
			case 1: //普通栏目
				unset($temp['cat_type']);
				unset($temp['go_url']);
				$url =  U('Home/'.$cat['remark'].'/lists',$temp);
				break;
			case 2: //封面栏目
				unset($temp['cat_type']);
				$url = U('Home/'.$cat['remark'].'/cover',$temp);
				break;
			case 3: //跳转
				$url = $temp['go_url'];
				break;
			case 4:
				unset($temp['cat_type']);
				unset($temp['go_url']);
				$url = U('Home/'.$cat['remark'].'/view',$temp);
				break;	
		}*/
		return array($url,$murl);
	}

	/**
	 * [get_one 读取一个字段的信息]
	 * @return [type] [description]
	*/
	public function get_one($cid)
	{
		$data = isset($this->cache[$cid])?$this->cache[$cid]:'';
		if($data)
		{
			$url = $this->get_url($data);
			$data['url'] = $url[0];
		}
	
		return $data;
	}




}