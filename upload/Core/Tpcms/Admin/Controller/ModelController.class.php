<?php
/**[模型管理]
 * @Author: happy
 * @Email:  976123967@qq.com
 * @Date:   2015-03-15 20:40:21
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:16:19
 */
namespace Admin\Controller;
class ModelController extends PublicController
{
	/**
	 * [index 模型列表]
	 * @return [type] [description]
	 */
	public function index()
	{
		$data = $this->logic->get_all();
		$this->assign('data',$data);
		$this->display();
	}
}
