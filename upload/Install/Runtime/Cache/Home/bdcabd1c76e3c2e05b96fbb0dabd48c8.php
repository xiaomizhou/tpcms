<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>欢迎使用thinkcms</title>
		<script type='text/javascript' src='/dcms/Core/Org/Jquery/jquery-1.8.2.min.js'></script>
	<link href='/dcms/Core/Org/hdjs/hdjs.css' rel='stylesheet' media='screen'>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/hdjs.min.js'></script>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/org/cal/lhgcalendar.min.js'></script>
	<script type='text/javascript'>
		MODULE='/dcms/install.php/Home'; //当前模块
		CONTROLLER='/dcms/install.php/Home/Index'; //当前控制器)
		ACTION='/dcms/install.php/Home/Index/index';//当前方法(方法)
		ROOT='/dcms'; //当前项目根路径
		PUBLIC= '/dcms/Install/Home/View/Public';//当前定义的Public目录
	</script>
			<link href='/dcms/Core/Org/bootstrap/css/bootstrap.min.css' rel='stylesheet' media='screen'>
			<script src='/dcms/Core/Org/bootstrap/js/bootstrap.min.js'></script>
			<!--[if lte IE 6]>
			<link rel="stylesheet" type="text/css" href="/dcms/Core/Org/bootstrap/ie6/css/bootstrap-ie6.css">
			<![endif]-->
			<!--[if lt IE 9]>
			<script src="/dcms/Core/Org/bootstrap/js/html5shiv.min.js"></script>
			<script src="/dcms/Core/Org/bootstrap/js/respond.min.js"></script>
			<![endif]-->

<link rel="stylesheet" type="text/css" href="/dcms/Install/Home/View/Public/Css/base.css" />
<script type="text/javascript" src="/dcms/Install/Home/View/Public/Js/base.js"></script>
</head>
<body>
<div class="container">
	<div class="container-title">
		<h1>欢迎使用TPCMS<small> 版本:<?php echo (C("tpcms_version")); ?></small></h1>

	</div>
 		<div>
 			<a href="<?php echo U('Index/step1');?>" class=" btn btn-primary btn-lg">开始安装</a>
 		</div>
		</div>
    </body>
</html>