/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50538
 Source Host           : localhost
 Source Database       : thinkcms

 Target Server Type    : MySQL
 Target Server Version : 50538
 File Encoding         : utf-8

 Date: 12/09/2014 23:25:57 PM
*/

SET NAMES utf8;{}
SET FOREIGN_KEY_CHECKS = 0;{}

-- ----------------------------
--  Table structure for `thinkcms_auth_group`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_auth_group`;{}
CREATE TABLE `thinkcms_auth_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='用户组表';{}

-- ----------------------------
--  Records of `thinkcms_auth_group`
-- ----------------------------
BEGIN;{}
INSERT INTO `thinkcms_auth_group` VALUES ('6', '普通管理员', '1', '19,20,21,22,24,39,40,41,46,26,27,28,29,42,47,30,31,33,49,48,50,34,37,35,36,43,44,45,51,52,53,54,55,56,57,58,59,60,94,61,93,62,63,64,68,65,66,67,69,70,71,72,73,74,75'), ('7', '客服管理员', '1', '19,20,21,22,24,39,40,26,27,28,29,30,31,33,49,34,37,35,36,43,44,45,53,54,57,58,59,60,61,93,62,63,65,66,67,69,70,71,74,75');{}
COMMIT;{}

-- ----------------------------
--  Table structure for `thinkcms_auth_group_access`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_auth_group_access`;{}
CREATE TABLE `thinkcms_auth_group_access` (
  `uid` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户表明细';{}

-- ----------------------------
--  Table structure for `thinkcms_auth_rule`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_auth_rule`;{}
CREATE TABLE `thinkcms_auth_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '',
  `pid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '级别',
  `isnavshow` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否显示导航',
  `sort` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=95 DEFAULT CHARSET=utf8 COMMENT='规则表';{}

-- ----------------------------
--  Records of `thinkcms_auth_rule`
-- ----------------------------
BEGIN;{}
INSERT INTO `thinkcms_auth_rule` VALUES ('19', 'admin', '后台项目', '1', '', '0', '1', '0', '1'), ('20', 'admin-index', '后台首页', '1', '', '19', '2', '0', '1'), ('21', 'admin-index-index', '欢迎界面', '1', '', '20', '3', '1', '1'), ('22', 'admin-article', '内容管理', '1', '', '19', '2', '1', '2'), ('39', 'admin-article-add', '添加文档', '1', '', '22', '3', '1', '2'), ('24', 'admin-article-index', '内容列表', '1', '', '22', '3', '0', '1'), ('26', 'admin-category', '栏目管理', '1', '', '19', '2', '1', '3'), ('27', 'admin-category-index', '栏目列表', '1', '', '26', '3', '1', '1'), ('28', 'admin-category-add', '添加栏目', '1', '', '26', '3', '1', '2'), ('29', 'admin-category-edit', '编辑栏目', '1', '', '26', '3', '0', '3'), ('30', 'admin-ad', '广告管理', '1', '', '19', '2', '1', '4'), ('31', 'admin-ad-index', '广告列表', '1', '', '30', '3', '1', '1'), ('41', 'admin-article-del', '删除文档', '1', '', '22', '3', '1', '4'), ('33', 'admin-ad-add', '添加广告', '1', '', '30', '3', '1', '2'), ('34', 'admin-position', '广告位置管理', '1', '', '19', '2', '1', '5'), ('35', 'admin-position-add', '添加广告位置', '1', '', '34', '3', '1', '2'), ('36', 'admin-position-edit', '编辑广告位置', '1', '', '34', '3', '0', '3'), ('37', 'admin-position-index', '广告位置列表', '1', '', '34', '3', '1', '1'), ('40', 'admin-article-edit', '编辑文档', '1', '', '22', '3', '1', '3'), ('42', 'admin-category-del', '删除栏目', '1', '', '26', '3', '1', '4'), ('43', 'admin-feedback', '留言管理', '1', '', '19', '2', '1', '6'), ('44', 'admin-feedback-index', '留言列表', '1', '', '43', '3', '1', '1'), ('45', 'admin-feedback-edit', '查看留言', '1', '', '43', '3', '1', '2'), ('46', 'admin-article-beachdelete', '文档批量操作', '1', '', '22', '3', '1', '5'), ('47', 'admin-category-beachdelete', '栏目批量操作', '1', '', '26', '3', '1', '5'), ('48', 'admin-ad-del', '广告删除', '1', '', '30', '3', '1', '4'), ('49', 'admin-ad-edit', '广告编辑', '1', '', '30', '3', '1', '3'), ('50', 'admin-ad-beachdelete', '广告批量操作', '1', '', '30', '3', '1', '5'), ('51', 'admin-feedback-del', '留言删除', '1', '', '43', '3', '1', '4'), ('52', 'admin-feedback-beachdelete', '留言批量操作', '1', '', '43', '3', '1', '5'), ('53', 'admin-attachment', '附件管理', '1', '', '19', '2', '1', '7'), ('54', 'admin-attachment-index', '附件列表', '1', '', '53', '3', '1', '1'), ('55', 'admin-attachment-del', '附件删除', '1', '', '53', '3', '1', '2'), ('56', 'admin-attachment-beachdelete', '附件批量操作', '1', '', '53', '3', '1', '3'), ('57', 'admin-model', '模型管理', '1', '', '19', '2', '1', '8'), ('58', 'admin-model-index', '模型列表', '1', '', '57', '3', '1', '1'), ('59', 'admin-model-add', '模型添加', '1', '', '57', '3', '1', '2'), ('60', 'admin-model-edit', '模型编辑', '1', '', '57', '3', '1', '3'), ('61', 'admin-field', '字段管理', '1', '', '19', '2', '1', '9'), ('62', 'admin-field-add', '字段添加', '1', '', '61', '3', '1', '2'), ('63', 'admin-field-edit', '字段编辑', '1', '', '61', '3', '1', '3'), ('64', 'admin-field-del', '字段删除', '1', '', '61', '3', '1', '4'), ('65', 'admin-flag', '属性管理', '1', '', '19', '2', '1', '10'), ('66', 'admin-flag-index', '属性列表', '1', '', '65', '3', '1', '1'), ('67', 'admin-user', '会员管理', '1', '', '19', '2', '1', '11'), ('68', 'admin-field-beachdelete', '字段批量操作', '1', '', '61', '3', '1', '5'), ('69', 'admin-user-index', '会员列表', '1', '', '67', '3', '1', '1'), ('70', 'admin-user-add', '会员添加', '1', '', '67', '3', '1', '2'), ('71', 'admin-user-edit', '会员编辑', '1', '', '67', '3', '1', '3'), ('72', 'admin-user-del', '会员删除', '1', '', '67', '3', '1', '4'), ('73', 'admin-user-beachdelete', '会员批量操作', '1', '', '67', '3', '1', '5'), ('74', 'admin-config', '系统管理', '1', '', '19', '2', '1', '12'), ('75', 'admin-config-index', '网站配置', '1', '', '74', '3', '1', '1'), ('76', 'admin-admin', '管理员管理', '1', '', '19', '2', '1', '13'), ('77', 'admin-admin-index', '管理员列表', '1', '', '76', '3', '1', '1'), ('78', 'admin-admin-add', '管理员添加', '1', '', '76', '3', '1', '2'), ('79', 'admin-admin-edit', '管理员编辑', '1', '', '76', '3', '1', '3'), ('80', 'admin-admin-del', '管理员删除', '1', '', '76', '3', '1', '4'), ('81', 'admin-admin-beachdelete', '管理员批量操作', '1', '', '76', '3', '1', '5'), ('82', 'admin-authgroup', '用户组管理', '1', '', '19', '2', '1', '14'), ('83', 'admin-authgroup-index', '用户组列表', '1', '', '82', '3', '1', '1'), ('84', 'admin-authgroup-add', '用户组添加', '1', '', '82', '3', '1', '2'), ('85', 'admin-authgroup-edit', '用户组编辑', '1', '', '82', '3', '1', '2'), ('86', 'admin-authgroup-rule', '分配权限', '1', '', '82', '3', '1', '4'), ('87', 'admin-authrule', '规则管理', '1', '', '19', '2', '1', '15'), ('88', 'admin-authrule-index', '规则列表', '1', '', '87', '3', '1', '1'), ('89', 'admin-authrule-add', '规则添加', '1', '', '87', '3', '1', '3'), ('90', 'admin-authrule-edit', '规则编辑', '1', '', '87', '3', '1', '2'), ('91', 'admin-authrule-del', '规则删除', '1', '', '87', '3', '1', '5'), ('92', 'admin-rule-beachdelete', '规则批量操作', '1', '', '87', '3', '1', '4'), ('93', 'admin-field-index', '字段列表', '1', '', '61', '3', '1', '1'), ('94', 'admin-model-del', '模型删除', '1', '', '57', '3', '1', '4');{}
COMMIT;{}


-- ----------------------------
--  Table structure for `thinkcms_ad`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_ad`;{}
CREATE TABLE `thinkcms_ad` (
  `aid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(50) NOT NULL DEFAULT '' COMMENT '广告名称',
  `name_en` char(50) NOT NULL DEFAULT '' COMMENT '英广告名称',
  `url` varchar(500) NOT NULL DEFAULT '' COMMENT '广告链接',
  `pic` varchar(200) NOT NULL DEFAULT '' COMMENT '广告图片',
  `pic_en` varchar(200) NOT NULL DEFAULT '' COMMENT '英广告图片',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `verifystate` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1 审核中，2审核通过 ，3不通过',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `position_psid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '广告位置id',
  `user_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户表关联',
  PRIMARY KEY (`aid`),
  KEY `fk_rb_ad_hd_position1_idx` (`position_psid`),
  KEY `fk_rb_ad_rb_user1_idx` (`user_uid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='广告表';{}

-- ----------------------------
--  Records of `thinkcms_ad`
-- ----------------------------
BEGIN;{}
INSERT INTO `thinkcms_ad` VALUES ('1', '首页','', '', './Data/Uploads/image/2014/12/09/5487135bbbf4e.jpg','', '1418138459', '2', '100', '1', '1'), ('2', '关于我们','', '', './Data/Uploads/image/2014/12/09/54871366c1435.jpg','', '1418138470', '2', '100', '2', '1'), ('3', '产品管理', '','', './Data/Uploads/image/2014/12/09/54871371710aa.jpg','', '1418138481', '2', '100', '3', '1'), ('4', '网站新闻', '','', './Data/Uploads/image/2014/12/09/5487137de1101.jpg', '1418138493', '','2', '100', '4', '1'), ('5', '人才招聘','', '', './Data/Uploads/image/2014/12/09/5487139023b24.jpg', '','1418138512', '2', '100', '5', '1'), ('6', '下载专区','', '', './Data/Uploads/image/2014/12/09/5487139bb42ca.jpg', '','1418138523', '2', '100', '6', '1'), ('7', '联系我们','', '', './Data/Uploads/image/2014/12/09/548713aa37413.jpg','', '1418138538', '2', '100', '7', '1');{}
COMMIT;{}

-- ----------------------------
--  Table structure for `thinkcms_article`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_article`;{}
CREATE TABLE `thinkcms_article` (
  `aid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_title` char(255) NOT NULL DEFAULT '' COMMENT '文档标题',
  `article_title_en` char(255) NOT NULL DEFAULT '' COMMENT '英文档标题',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `click` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点击次数',
  `flag` set('推荐','头条','图文') DEFAULT NULL COMMENT '属性',
  `is_top` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0不置顶 ，1置顶',
  `keywords` char(80) NOT NULL DEFAULT '' COMMENT '关键字',
  `keywords_en` char(80) NOT NULL DEFAULT '' COMMENT '英关键字',
  `description` varchar(500) NOT NULL DEFAULT '' COMMENT '描述',
  `description_en` varchar(500) NOT NULL DEFAULT '' COMMENT '英描述',
  `file` varchar(200) NOT NULL DEFAULT '' COMMENT '下载地址',
  `pic` varchar(200) NOT NULL DEFAULT '' COMMENT '原图',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `edittime` int(11) NOT NULL DEFAULT '0' COMMENT '编辑时间',
  `resource` char(20) NOT NULL DEFAULT '' COMMENT '来源',
  `verifystate` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1审核中  2 审核通过  3审核失败',
  `tag` varchar(500) NOT NULL DEFAULT '' COMMENT 'tag标签',
  `seo_title` char(255) NOT NULL DEFAULT '' COMMENT 'seo标题',
  `tpl` varchar(45) NOT NULL DEFAULT '' COMMENT '模板',
  `user_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户表关联',
  `category_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '栏目表关联',
  PRIMARY KEY (`aid`),
  KEY `fk_rb_article_rb_user1_idx` (`user_uid`),
  KEY `fk_rb_article_rb_category1_idx` (`category_cid`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='文档表';{}

-- ----------------------------
--  Records of `thinkcms_article`
-- ----------------------------
BEGIN;{}
INSERT INTO `thinkcms_article` VALUES ('1', '公司简介','', '100', '132', '', '0', '', '', '', '','','', '1418133273', '1418133273', '', '2', '', '', '', '1', '7'), ('2', '公司团队','', '100', '106', '', '0', '', '', '', '','','', '1418133289', '1418133289', '', '2', '', '', '', '1', '8'), ('3', '人才理念','', '100', '107', '', '0', '', '', '', '','','', '1418134234', '1418134234', '', '2', '', '', '', '1', '10'), ('4', '人才招聘','', '100', '100', '', '0', '', '', '', '', '','','1418134242', '1418134242', '', '2', '', '', '', '1', '11'), ('5', '联系地址','', '100', '106', '', '0', '', '', '', '','','', '1418134250', '1418134250', '', '2', '', '', '', '1', '9'), ('6', '新闻标题1','', '100', '100', '推荐', '0', '', '', '', '','','', '1418054400', '1418135522', '来源', '2', '', '', '', '1', '12'), ('7', '新闻标题2','', '100', '101', '推荐', '0', '', '', '', '','','', '-28800', '1418135409', '来源', '2', '', '', '', '1', '12'), ('8', '新闻标题3','', '100', '100', '推荐', '0', '', '', '', '','','', '1418054400', '1418135516', '来源', '2', '', '', '', '1', '12'), ('9', '新闻标题4','',  '100', '100', '推荐', '0', '', '', '', '', '', '', '1418054400', '1418135511', '来源', '2', '', '', '', '1', '12'), ('10', '新闻标题5', '','100', '100', '推荐', '0', '', '', '', '','','', '1418054400', '1418135505', '来源', '2', '', '', '', '1', '12'), ('11', '新闻标题6','', '100', '100', '', '0', '', '', '', '', '','','1418054400', '1418135536', '来源', '2', '', '', '', '1', '13'), ('12', '新闻标题7', '','100', '106', '', '0', '', '', '', '', '','','1418054400', '1418135451', '来源', '2', '', '', '', '1', '13'), ('13', '手册下载','', '100', '100', '', '0', '', '','','', './Data/Uploads/file/2014/12/09/5487092823ec3.jpg', '', '1418135848', '1418135848', '', '2', '', '', '', '1', '14'), ('14', '系统下载', '','100', '100', '', '0', '', '','','', './Data/Uploads/file/2014/12/09/5487093d21615.jpg', '', '1418054400', '1418135881', '', '2', '', '', '', '1', '15'), ('15', '产品名称1','', '100', '101', '推荐', '0', '', '', '','','', './Data/Uploads/image/2014/12/09/54870b4cec2f4.jpg', '1418136396', '1418136396', '', '2', '', '', '', '1', '16'), ('16', '产品名称2','', '100', '109', '推荐', '0', '', '', '', '','','./Data/Uploads/image/2014/12/09/54870b5dedd24.jpg', '1418136413', '1418136413', '', '2', '', '', '', '1', '16'), ('17', '首页公司简介','', '100', '100', '', '0', '', '', '', '', '','','1418137500', '1418137500', '', '2', '', '', '', '1', '18');{}
COMMIT;{}

-- ----------------------------
--  Table structure for `thinkcms_article_about`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_article_about`;{}
CREATE TABLE `thinkcms_article_about` (
  `article_aid` int(10) unsigned NOT NULL COMMENT '主表关联外键',
  `body` text COMMENT '详细内容',
  KEY `fk_rb_article_data_rb_article1_idx` (`article_aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='公司介绍';{}

-- ----------------------------
--  Records of `thinkcms_article_about`
-- ----------------------------
BEGIN;{}
INSERT INTO `thinkcms_article_about` VALUES ('1', '<span style=\\\"line-height:1.5;\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;是一家专业从事互联网视觉营销、创意交互、品牌策划以及优化推广的专业服务机构。作为一家专业的品牌视觉营销公司，在提供优质的网站设计建设与网页设计制作服务的同时，也为企业提供整体的互联网品牌形象改造管理解决方案。\r\n在以提高企业营销目的基础之上进行个性化的定制服务。意将视觉营销、品牌传播、互动体验、创意设计、策略执行五者有机融合，通过采用多种的传播手段及互动产品，为企业的品牌推广提供优质高效的服务。\r\n我们认为，一个能够健康发展的品牌就如一颗枝叶繁茂的大树，创意的绿叶是品牌的标志，而以销售为目的功能需求更是根脉扎根市场汲取养分的重要利器。所以我们追求作品在美观和销售功能上的平衡点，美观而不失功能，易用而不失创意，是我们的首要设计守则。&nbsp;</span> \r\n<p>\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;我们旨在为企业的价值寻找突破点，以提供优质的服务及解决方案为企业成就品牌。我们作为一家专业的视觉营销创意设计公司，具有完备且专业的项目流程。\r\n从需求调研、创意设计、成稿审核、演示讲解都具有专业且规范的流程行为指导，力求在双方互通互信的基础之上，将服务做到最优，以优质的服务铸就品牌的成长。与客户一起创造共同价值，与客户共同发展。\r\n</p>'), ('2', '公司团队公司团队公司团队公司团队公司团队公司团队公司团队公司团队公司团队公司团队公司团队'), ('17', '物流有限公司通过全体员工的不懈努力，现已成功组建了一支组织健全、经验丰富、设施完善，具有一定影响力的专业物流运输车队。 \r\n我们主要经营南京至全国各地的公路运输线路，同时提供超一流的货物包装及现代化仓储、物流服务。为客户节约开支，安全经营，减少风险。您的满意，我的追求。');{}
COMMIT;{}

-- ----------------------------
--  Table structure for `thinkcms_article_attr`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_article_attr`;{}
CREATE TABLE `thinkcms_article_attr` (
  `article_attr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attr_attr_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文档属性表关联外键',
  `category_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '栏目关联外键',
  `article_aid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文档关联外键',
  `type_typeid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '属性值',
  `attr_value` varchar(100) NOT NULL DEFAULT '' COMMENT '属性值',
  `attr_value_attr_value_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '属性值表关联字段',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '展示类型 1单选 2多选',
  `is_pic` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`article_attr_id`),
  KEY `fk_thinkcms_article_attr_thinkcms_attr1_idx` (`attr_attr_id`),
  KEY `fk_thinkcms_article_attr_thinkcms_category1_idx` (`category_cid`),
  KEY `fk_thinkcms_article_attr_thinkcms_article1_idx` (`article_aid`),
  KEY `fk_thinkcms_article_attr_thinkcms_type1_idx` (`type_typeid`),
  KEY `fk_thinkcms_article_attr_thinkcms_attr_value1_idx` (`attr_value_attr_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文档和属性表关联中间表';{}

-- ----------------------------
--  Table structure for `thinkcms_article_contact`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_article_contact`;{}
CREATE TABLE `thinkcms_article_contact` (
  `article_aid` int(10) unsigned NOT NULL COMMENT '主表关联外键',
  `body` text COMMENT '详细内容',
  KEY `fk_rb_article_data_rb_article1_idx` (`article_aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='联系我们';{}

-- ----------------------------
--  Records of `thinkcms_article_contact`
-- ----------------------------
BEGIN;{}
INSERT INTO `thinkcms_article_contact` VALUES ('5', '联系地址联系地址联系地址联系地址联系地址联系地址联系地址');{}
COMMIT;{}



-- ----------------------------
--  Table structure for `thinkcms_article_download`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_article_download`;{}
CREATE TABLE `thinkcms_article_download` (
  `article_aid` int(10) unsigned NOT NULL COMMENT '主表关联外键',
  `body` text COMMENT '详细内容',
  KEY `fk_rb_article_data_rb_article1_idx` (`article_aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='下载专区';{}

-- ----------------------------
--  Records of `thinkcms_article_download`
-- ----------------------------
BEGIN;{}
INSERT INTO `thinkcms_article_download` VALUES ('13', ''), ('14', '');{}
COMMIT;{}

-- ----------------------------
--  Table structure for `thinkcms_article_news`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_article_news`;{}
CREATE TABLE `thinkcms_article_news` (
  `article_aid` int(10) unsigned NOT NULL COMMENT '主表关联外键',
  `body` text COMMENT '详细内容',
  `author` varchar(255) NOT NULL DEFAULT '' COMMENT '作者',
  `resource` varchar(255) NOT NULL DEFAULT '' COMMENT '来源',
  KEY `fk_rb_article_data_rb_article1_idx` (`article_aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='新闻模型';{}

-- ----------------------------
--  Records of `thinkcms_article_news`
-- ----------------------------
BEGIN;{}
INSERT INTO `thinkcms_article_news` VALUES ('6', '新闻标题1新闻标题1新闻标题1新闻标题1新闻标题1', '作者', '来源'), ('7', '新闻标题2新闻标题2新闻标题2新闻标题2', '作者', '来源'), ('8', '新闻标题3新闻标题3新闻标题3新闻标题3', '作者', '来源'), ('9', '新闻标题4新闻标题4新闻标题4新闻标题4', '作者', '来源'), ('10', '新闻标题5新闻标题5新闻标题5', '作者', '来源'), ('11', '新闻标题6新闻标题6新闻标题6新闻标题6', '作者', '来源'), ('12', '新闻标题7新闻标题7新闻标题7新闻标题7', '作者', '来源');{}
COMMIT;{}

-- ----------------------------
--  Table structure for `thinkcms_article_pic`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_article_pic`;{}
CREATE TABLE `thinkcms_article_pic` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `big` varchar(200) NOT NULL DEFAULT '' COMMENT '大图',
  `medium` varchar(200) NOT NULL DEFAULT '' COMMENT '中图',
  `small` varchar(200) NOT NULL DEFAULT '' COMMENT '小图',
  `article_aid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文档关联外键',
  `attr_value_attr_value_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '属性值关联外键',
  PRIMARY KEY (`id`),
  KEY `fk_rb_pic_rb_article1_idx` (`article_aid`),
  KEY `fk_thinkcms_article_pic_thinkcms_attr_value1_attr_value_idx` (`attr_value_attr_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='图集';{}

-- ----------------------------
--  Table structure for `thinkcms_article_products`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_article_products`;{}
CREATE TABLE `thinkcms_article_products` (
  `article_aid` int(10) unsigned NOT NULL COMMENT '主表关联外键',
  `body` text COMMENT '详细内容',
  KEY `fk_rb_article_data_rb_article1_idx` (`article_aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='产品管理';{}

-- ----------------------------
--  Records of `thinkcms_article_products`
-- ----------------------------
BEGIN;{}
INSERT INTO `thinkcms_article_products` VALUES ('15', '详细内容详细内容详细内容详细内容详细内容'), ('16', '详细内容详细内容详细内容详细内容');{}
COMMIT;{}

-- ----------------------------
--  Table structure for `thinkcms_article_recruitment`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_article_recruitment`;{}
CREATE TABLE `thinkcms_article_recruitment` (
  `article_aid` int(10) unsigned NOT NULL COMMENT '主表关联外键',
  `body` text COMMENT '详细内容',
  KEY `fk_rb_article_data_rb_article1_idx` (`article_aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='人才招聘';{}

-- ----------------------------
--  Records of `thinkcms_article_recruitment`
-- ----------------------------
BEGIN;{}
INSERT INTO `thinkcms_article_recruitment` VALUES ('3', '人才理念人才理念人才理念人才理念人才理念人才理念人才理念人才理念'), ('4', '人才招聘人才招聘人才招聘人才招聘人才招聘人才招聘');{}
COMMIT;{}

-- ----------------------------
--  Table structure for `thinkcms_attr`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_attr`;{}
CREATE TABLE `thinkcms_attr` (
  `attr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attr_name` varchar(30) NOT NULL DEFAULT '' COMMENT '类型说明',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1 单选， 2多选',
  `type_typeid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文档类型关联外键',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `is_pic` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否需要有图集 1需要   0 不需要',
  PRIMARY KEY (`attr_id`),
  KEY `fk_thinkcms_attr_thinkcms_type1_idx` (`type_typeid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文档属性，用于筛选';{}

-- ----------------------------
--  Table structure for `thinkcms_attr_value`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_attr_value`;{}
CREATE TABLE `thinkcms_attr_value` (
  `attr_value_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attr_value_name` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `attr_value` varchar(100) NOT NULL DEFAULT '' COMMENT '属性值',
  `attr_attr_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文档属性关联外键',
  PRIMARY KEY (`attr_value_id`),
  KEY `fk_think_attr_value_think_attr1_idx` (`attr_attr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文档类型默认值';{}

-- ----------------------------
--  Table structure for `thinkcms_category`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_category`;{}
CREATE TABLE `thinkcms_category` (
  `cid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cname` char(255) NOT NULL DEFAULT '' COMMENT '分类名称',
  `cname_en` char(255) NOT NULL DEFAULT '' COMMENT '英分类名称',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `cat_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1封面 2单一内容 3普通 4跳转',
  `go_url` varchar(500) NOT NULL DEFAULT '' COMMENT '跳转地址',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `pic` varchar(200) NOT NULL DEFAULT '' COMMENT '栏目图片',
  `page` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '每一页记录数',
  `go_child` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 不跳转到子分类 1 跳转到子分类',
  `seo_title` varchar(200) NOT NULL DEFAULT '' COMMENT 'seo标题',
  `keywords` char(80) NOT NULL DEFAULT '' COMMENT '关键字',
  `keywords_en` char(80) NOT NULL DEFAULT '' COMMENT '英关键字',
  `description` varchar(500) NOT NULL DEFAULT '' COMMENT '描述',
  `description_en` varchar(500) NOT NULL DEFAULT '' COMMENT '英描述',
  `default_tpl` char(20) NOT NULL DEFAULT '' COMMENT '封面模板',
  `list_tpl` char(20) NOT NULL DEFAULT '' COMMENT '列表模板',
  `view_tpl` char(20) NOT NULL DEFAULT '' COMMENT '视图模板',
  `model_mid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '模型关联外键',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '控制器',
  `type_typeid` int(10) unsigned NOT NULL COMMENT '文档类型管理外键',
  `target` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1当前窗口 2 新窗口',
  `is_show` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否显示 1 显示 0 不显示',
  PRIMARY KEY (`cid`),
  KEY `fk_rb_category_rb_model1_idx` (`model_mid`),
  KEY `fk_thinkcms_category_thinkcms_type1_idx` (`type_typeid`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='栏目表';{}

-- ----------------------------
--  Records of `thinkcms_category`
-- ----------------------------
BEGIN;{}
INSERT INTO `thinkcms_category` VALUES ('1', '关于我们','', '0', '4', '', '100', '', '20', '1', '', '', '', '','','default', 'lists', 'view', '2', 'About', '0', '1','1'), ('2', '产品管理','', '0', '1', '', '100', '', '20', '0', '', '', '','','', 'default', 'lists', 'view', '3', 'Products', '0', '1','1'), ('3', '网站新闻', '','0', '1', '', '100', '', '20', '0', '', '', '','','', 'default', 'lists', 'view', '1', 'News', '0', '1','1'), ('4', '人才招聘','', '0', '4', '', '100', '', '20', '1', '', '', '','','', 'default', 'lists', 'view', '4', 'Recruitment', '0', '1','1'), ('5', '下载专区','', '0', '1', '', '100', '', '20', '0', '', '', '','','', 'default', 'lists', 'view', '5', 'Download', '0', '1','1'), ('6', '联系我们', '','0', '4', '', '100', '', '20', '1', '', '', '','','', 'default', 'lists', 'view', '6', 'Contact', '0', '1','1'), ('7', '公司简介', '','1', '4', '', '100', '', '20', '1', '', '', '', '','','default', 'lists', 'view', '2', 'About', '0', '1','1'), ('8', '公司团队','', '1', '4', '', '100', '', '20', '1', '', '', '','','', 'default', 'lists', 'view', '2', 'About', '0', '1','1'), ('9', '联系地址','', '6', '4', '', '100', '', '20', '1', '', '', '','','', 'default', 'lists', 'view', '6', 'Contact', '0', '1','1'), ('10', '人才理念','', '4', '4', '', '100', '', '20', '1', '', '', '', '','','default', 'lists', 'view', '4', 'Recruitment', '0', '1','1'), ('11', '人才招聘','', '4', '4', '', '100', '', '20', '1', '', '', '', '','','default', 'lists', 'view', '4', 'Recruitment', '0', '1','1'), ('12', '公司新闻', '','3', '1', '', '100', '', '20', '0', '', '', '','','', 'default', 'lists', 'view', '1', 'News', '0', '1','1'), ('13', '行业新闻','', '3', '1', '', '100', '', '20', '0', '', '', '','','', 'default', 'lists', 'view', '1', 'News', '0', '1','1'), ('14', '产品手册','', '5', '1', '', '100', '', '20', '0', '', '', '','','', 'default', 'lists', 'view', '5', 'Download', '0', '1','1'), ('15', '产品系统','', '5', '1', '', '100', '', '20', '0', '', '', '','','', 'default', 'lists', 'view', '5', 'Download', '0', '1','1'), ('16', '产品分类1','', '2', '1', '', '100', '', '20', '0', '', '', '', '','','default', 'lists', 'view', '3', 'Products', '0', '1','1'), ('17', '产品分类2','', '2', '1', '', '100', '', '20', '0', '', '', '', '','','default', 'lists', 'view', '3', 'Products', '0', '1','1'), ('18', '首页公司简介','', '0', '4', '', '100', '', '20', '0', '', '', '', '','','default', 'lists', 'view', '2', 'Index', '0', '1','1');{}
COMMIT;{}

-- ----------------------------
--  Table structure for `thinkcms_config`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_config`;{}
CREATE TABLE `thinkcms_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` char(100) NOT NULL DEFAULT '' COMMENT '引用代码',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '中文说明',
  `body` varchar(500) NOT NULL DEFAULT '' COMMENT '具体信息',
  `config_type` tinyint(4) NOT NULL DEFAULT '2' COMMENT '1图片 2单行文本 3 多行文本',
  `group` enum('基本设置','更多设置') NOT NULL DEFAULT '基本设置',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='网站配置';{}

-- ----------------------------
--  Records of `thinkcms_config`
-- ----------------------------
BEGIN;{}
INSERT INTO `thinkcms_config` VALUES ('1', 'cfg_name', '网站标题', '绿黑色物流网站设计G', '2', '基本设置', '1'), ('2', 'cfg_keywords', '关键字', '关键字', '3', '基本设置', '2'), ('3', 'cfg_description', '描述', '描述', '3', '基本设置', '3'), ('4', 'cfg_copyright', '底部信息', 'Copyright@ 2013-2014 \r\n版权所有：绿黑色物流网站设计G', '3', '基本设置', '4'), ('11', 'cfg_address', '地址', '软件园二期望海路2号', '2', '基本设置', '6'), ('9', 'cfg_image', '图片上传格式', 'gif|png|jpg|jpeg', '3', '更多设置', '2'), ('6', 'cfg_logo', 'LOGO', './Data/Uploads/image/2015/01/14/54b67e83543e9.png', '1', '更多设置', '1'), ('7', 'cfg_icp', '备案号', '备案号', '2', '基本设置', '5'), ('8', 'cfg_count', '引用', '', '3', '更多设置', '4'), ('10', 'cfg_file', '文件上传格式', 'doc|docx|ppt|pptx|xls|xlsx|zip|rar|7z|gif|png|jpg|jpeg|pdf', '3', '更多设置', '3'), ('12', 'cfg_tel', '电话', '0755-88888888', '2', '基本设置', '7'), ('13', 'cfg_email', '邮箱', 'web@aaaa.com', '2', '基本设置', '8'),('14', 'cfg_pic_small_width', '图集小图宽', '55', '2', '更多设置', '6'),('15', 'cfg_pic_small_height', '图集小图高', '55', '2', '更多设置', '7'), ('16', 'cfg_pic_medium_width', '图集中图宽', '300', '2', '更多设置', '8'), ('17', 'cfg_pic_medium_height', '图集中图高', '300', '2', '更多设置', '9'), ('18', 'cfg_map', '百度地图地址', '深圳市腾讯大厦', '2', '更多设置', '10');{}
COMMIT;{}

-- ----------------------------
--  Table structure for `thinkcms_feedback`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_feedback`;{}
CREATE TABLE `thinkcms_feedback` (
  `fd_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `theme` char(100) NOT NULL DEFAULT '' COMMENT '主题',
  `body` text COMMENT '内容',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '留言时间',
  `people` char(20) NOT NULL DEFAULT '' COMMENT '联系人',
  `email` varchar(60) NOT NULL DEFAULT '' COMMENT '电子邮件',
  `tel` char(10) NOT NULL DEFAULT '' COMMENT '固定电话',
  `phone` char(11) NOT NULL DEFAULT '' COMMENT '手机',
  `lookstate` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1没有看 2已经阅读',
  `showstate` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0不显示 1显示',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父级',
  `user_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员表关联外键',
  PRIMARY KEY (`fd_id`),
  KEY `fk_hd_feedback_rb_user1_idx` (`user_uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='留言表';{}

-- ----------------------------
--  Table structure for `thinkcms_link`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_link`;{}
CREATE TABLE `thinkcms_link` (
  `lid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(20) NOT NULL DEFAULT '' COMMENT '链接名称',
  `url` varchar(500) NOT NULL DEFAULT '' COMMENT '链接地址',
  `logo` varchar(200) NOT NULL DEFAULT '' COMMENT '链接logo',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `verifystate` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1审核中，2审核通过，3失败',
  `people` char(20) NOT NULL DEFAULT '' COMMENT '申请人姓名',
  `phone` char(11) NOT NULL DEFAULT '' COMMENT '申请人联系手机',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `user_uid` int(10) unsigned NOT NULL COMMENT '用户表关联',
  PRIMARY KEY (`lid`),
  KEY `fk_rb_link_rb_user_idx` (`user_uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='友情链接表';{}

-- ----------------------------
--  Table structure for `thinkcms_model`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_model`;{}
CREATE TABLE `thinkcms_model` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(50) NOT NULL DEFAULT '' COMMENT '模型名称(英文)',
  `remark` char(50) NOT NULL DEFAULT '' COMMENT '中文说明',
  PRIMARY KEY (`mid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='模型表';{}

-- ----------------------------
--  Records of `thinkcms_model`
-- ----------------------------
BEGIN;{}
INSERT INTO `thinkcms_model` VALUES ('1', 'news', '新闻模型'), ('2', 'about', '公司介绍'), ('3', 'products', '产品管理'), ('4', 'recruitment', '人才招聘'), ('5', 'download', '下载专区'), ('6', 'contact', '联系我们');{}
COMMIT;{}

-- ----------------------------
--  Table structure for `thinkcms_model_field`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_model_field`;{}
CREATE TABLE `thinkcms_model_field` (
  `fid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fname` char(20) NOT NULL DEFAULT '' COMMENT '字段名称英文',
  `title` char(50) NOT NULL DEFAULT '' COMMENT '字段说明',
  `validate` varchar(100) NOT NULL DEFAULT '' COMMENT '正则',
  `require` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 选填 1必填',
  `show_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1 文本 ，2多行文本 ，3 html ，4 单选框 ，5下拉框，6多选框 ，7文件上传框，8图片上传框 ， 9地区联动',
  `show_lists` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0显示列表页 1显示列表页',
  `is_system` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 不是系统字段 1 系统字段',
  `is_disabled` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '1禁用 0正常',
  `model_mid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '模型表关联外键',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `error` varchar(100) NOT NULL DEFAULT '' COMMENT '错误提示',
  PRIMARY KEY (`fid`),
  KEY `fk_rb_model_field_rb_model1_idx` (`model_mid`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='字段表';{}

-- ----------------------------
--  Records of `thinkcms_model_field`
-- ----------------------------
BEGIN;{}
INSERT INTO `thinkcms_model_field` VALUES ('1', 'article_aid', '关联字段', '', '1', '1', '0', '1', '0', '1', '1', ''), ('2', 'body', '详细内容', '', '0', '3', '0', '0', '0', '1', '900', ''), ('3', 'article_aid', '关联字段', '', '0', '1', '0', '1', '0', '2', '1', ''), ('4', 'body', '详细内容', '', '0', '3', '1', '0', '0', '2', '900', ''), ('5', 'article_aid', '关联字段', '', '0', '1', '0', '1', '0', '3', '1', ''), ('6', 'body', '详细内容', '', '0', '3', '0', '0', '0', '3', '900', ''), ('7', 'article_aid', '关联字段', '', '0', '1', '0', '1', '0', '4', '1', ''), ('8', 'body', '详细内容', '', '0', '3', '0', '0', '0', '4', '900', ''), ('9', 'article_aid', '关联字段', '', '0', '1', '0', '1', '0', '5', '1', ''), ('10', 'body', '详细内容', '', '0', '3', '0', '0', '0', '5', '900', ''), ('11', 'article_aid', '关联字段', '', '0', '1', '0', '1', '0', '6', '1', ''), ('12', 'body', '详细内容', '', '0', '3', '0', '0', '0', '6', '900', ''), ('13', 'author', '作者', '', '0', '1', '0', '0', '0', '1', '2', ''), ('14', 'resource', '来源', '', '0', '1', '0', '0', '0', '1', '2', '');{}
COMMIT;{}

-- ----------------------------
--  Table structure for `thinkcms_model_field_value`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_model_field_value`;{}
CREATE TABLE `thinkcms_model_field_value` (
  `fv_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_value` varchar(60) NOT NULL DEFAULT '' COMMENT '默认值',
  `field_fid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '字段表关联外键',
  PRIMARY KEY (`fv_id`),
  KEY `fk_rb_model_field_value_rb_model_field1_idx` (`field_fid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='字段默认值表';{}

-- ----------------------------
--  Records of `thinkcms_model_field_value`
-- ----------------------------
BEGIN;{}
INSERT INTO `thinkcms_model_field_value` VALUES ('1', '', '13'), ('2', '', '14'), ('3', '', '4');{}
COMMIT;{}

-- ----------------------------
--  Table structure for `thinkcms_position`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_position`;{}
CREATE TABLE `thinkcms_position` (
  `psid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position_name` char(100) NOT NULL DEFAULT '' COMMENT '位置名称',
  `width` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '宽度',
  `height` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '高度',
  PRIMARY KEY (`psid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='广告位置';{}

-- ----------------------------
--  Records of `thinkcms_position`
-- ----------------------------
BEGIN;{}
INSERT INTO `thinkcms_position` VALUES ('1', '首页', '960', '380'), ('2', '关于我们', '960', '380'), ('3', '产品管理', '960', '380'), ('4', '网站新闻', '960', '380'), ('5', '人才招聘', '960', '380'), ('6', '下载专区', '960', '380'), ('7', '联系我们', '960', '380');{}
COMMIT;{}



-- ----------------------------
--  Table structure for `thinkcms_type`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_type`;{}
CREATE TABLE `thinkcms_type` (
  `typeid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typename` varchar(30) NOT NULL DEFAULT '' COMMENT '类型名称',
  PRIMARY KEY (`typeid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='文档类型';{}

-- ----------------------------
--  Table structure for `thinkcms_upload`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_upload`;{}
CREATE TABLE `thinkcms_upload` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ext` varchar(45) NOT NULL DEFAULT '' COMMENT '文件扩展名',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '文件名称',
  `path` varchar(255) NOT NULL,
  `size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `article_aid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文档关联表外键',
  `user_uid` int(10) unsigned NOT NULL COMMENT '用户表关联外键',
  PRIMARY KEY (`id`),
  KEY `fk_thinkcms_upload_thinkcms_article1_idx` (`article_aid`),
  KEY `fk_thinkcms_upload_thinkcms_user1_idx` (`user_uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='编辑器图片表';{}

-- ----------------------------
--  Table structure for `thinkcms_user`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_user`;{}
CREATE TABLE `thinkcms_user` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` char(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` char(32) NOT NULL DEFAULT '' COMMENT '密码',
  `login_ip` char(20) NOT NULL DEFAULT '' COMMENT '登录IP',
  `login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录时间',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `role` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1管理员2会员',
  `times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `is_lock` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否锁定 0正常,1锁定',
  `grade_gid` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '会员的等级',
  `nickname` varchar(30) NOT NULL DEFAULT '' COMMENT '昵称',
  `email` varchar(60) NOT NULL DEFAULT '' COMMENT '邮箱',
  PRIMARY KEY (`uid`),
  KEY `fk_rb_user_rb_grade1_idx` (`grade_gid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户表';{}



-- ----------------------------
--  Table structure for `thinkcms_user_baseinfo`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_user_baseinfo`;{}
CREATE TABLE `thinkcms_user_baseinfo` (
  `bid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `realname` char(20) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `sex` enum('男','女') NOT NULL DEFAULT '男' COMMENT '性别',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `qq` char(15) NOT NULL DEFAULT '' COMMENT 'qq',
  `email` char(60) NOT NULL DEFAULT '' COMMENT '邮箱地址',
  `phone` char(11) NOT NULL DEFAULT '' COMMENT '手机号码',
  `face` varchar(200) NOT NULL DEFAULT '' COMMENT '头像',
  `user_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'user表关联外键',
  PRIMARY KEY (`bid`),
  KEY `fk_rb_user_baseinfo_rb_user1_idx` (`user_uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户基本信息表';{}

-- ----------------------------
--  Table structure for `thinkcms_user_comment`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_user_comment`;{}
CREATE TABLE `thinkcms_user_comment` (
  `cmid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(500) NOT NULL DEFAULT '' COMMENT '评论内容',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论时间',
  `verifystate` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1审核中 2 审核通过  3 不通过',
  `article_aid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文章主表关联外键',
  `user_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户表关联外键',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `score` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评分',
  PRIMARY KEY (`cmid`),
  KEY `fk_rb_user_comment_rb_article1_idx` (`article_aid`),
  KEY `fk_rb_user_comment_rb_user1_idx` (`user_uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='评论表';{}

-- ----------------------------
--  Table structure for `thinkcms_user_grade`
-- ----------------------------
DROP TABLE IF EXISTS `thinkcms_user_grade`;{}
CREATE TABLE `thinkcms_user_grade` (
  `gid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gname` char(20) NOT NULL DEFAULT '' COMMENT '会员等级',
  PRIMARY KEY (`gid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='会员等级';{}




SET FOREIGN_KEY_CHECKS = 1;{}