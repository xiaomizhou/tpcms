<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>添加栏目</title>
	<script type='text/javascript' src='/dcms/Core/Org/Jquery/jquery-1.8.2.min.js'></script>
	<link href='/dcms/Core/Org/hdjs/hdjs.css' rel='stylesheet' media='screen'>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/hdjs.min.js'></script>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/org/cal/lhgcalendar.min.js'></script>
	<script type='text/javascript'>
		MODULE='/dcms/index.php/Admin'; //当前模块
		CONTROLLER='/dcms/index.php/Admin/Category'; //当前控制器)
		ACTION='/dcms/index.php/Admin/Category/add';//当前方法(方法)
		ROOT='/dcms'; //当前项目根路径
		PUBLIC= '/dcms/Core/Tpcms/Admin/View/Public';//当前定义的Public目录
	</script>
	<script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.base.js"></script><script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.category.js"></script>
	<link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.base.css" />
</head>
<body>
	<form action="" method="post" enctype='multipart/form-data' class='hd-form' name="form">
		
			<div class="hd-menu-list">
				<ul>
					<li>
						<a href="<?php echo U('Category/index');?>" >栏目列表</a>
					</li>
					<li class='active'>
						<a href="javascript:;" >添加栏目</a>
					</li>
					
				</ul>
			</div>

			<div class="right_content">
				<div class="hd-tab">
					<ul class="hd-tab-menu">
						<li lab='base' class="active">
							<a href="#">基本配置</a>
						</li>
						<li lab='view'>
							<a href="#">模板配置</a>
						</li>
					</ul>
					<div class="hd-tab-content">
						<div lab='base' class='hd-table-area'>
							<table class='hd-table hd-table-form hd-form'>
								<tbody>
									<tr>
										<th class="hd-w100">
											模型
											<span class="star">*</span>
										</th>
										<td>

											<?php if($parent): ?><input type="hidden" name="model_mid" value="<?php echo ($parent["model_mid"]); ?>" />
												<?php echo ($parent["model"]); ?>
												<?php else: ?>
												<select name="model_mid">
													<!-- <option value="0">请选择模型</option>
												-->
												<?php if(is_array($model)): foreach($model as $key=>$v): ?><option  value="<?php echo ($v["mid"]); ?>"><?php echo ($v["remark"]); ?></option><?php endforeach; endif; ?>
												</select><?php endif; ?>
										</td>
									</tr>
									<tr>
										<th>
											文档类型
											
										</th>
										<td>

											<select name="type_typeid">
												<option value="0">请选择文档类型</option>
												<?php if(is_array($type)): foreach($type as $key=>$v): ?><option  value="<?php echo ($v["typeid"]); ?>" <?php if(isset($parent['type_typeid'])&& $v['typeid'] == $parent['type_typeid']): ?>selected="selected"<?php endif; ?>
													><?php echo ($v["typename"]); ?>
												</option><?php endforeach; endif; ?>
										</select>

									</td>
									</tr>
									<tr>
										<th >
											上级分类
											
										</th>
										<td>
											<?php if($parent): ?><input type="hidden" name="pid" value="<?php echo ($parent["cid"]); ?>" />
												<?php echo ($parent["cname"]); ?>
												<?php else: ?>
												<select name="pid" >
													<option value="0">一级分类</option>
													<?php if(is_array($category)): foreach($category as $key=>$v): ?><option value="<?php echo ($v["cid"]); ?>"><?php echo ($v["_name"]); ?></option><?php endforeach; endif; ?>
												</select><?php endif; ?>
										</td>
									</tr>
									<tr>
										<th >
										分类名称
										<span class="star">*</span>
										</th>
										<td>
											<input type="text" name="cname"  class='hd-w200'/>
										</td>
									</tr>
									<tr>
										<th >
										(英)分类名称
										<span class="star">*</span>
										</th>
										<td>
											<input type="text" name="cname_en"  class='hd-w200'/>
										</td>
									</tr>

									<tr >
										<th>栏目图片</th>
										<td>
											<input type="file" name='pic'/>
										</td>
									</tr>

									<tr>
										<th>分类关键字</th>
										<td>
											<textarea name="keywords" class='hd-w500 hd-h100'></textarea>
										</td>
									</tr>
									<tr>
										<th>(英)分类关键字</th>
										<td>
											<textarea name="keywords_en" class='hd-w500 hd-h100'></textarea>
										</td>
									</tr>

									<tr>
										<th>分类描述</th>
										<td>
											<textarea name="description" class='hd-w500 hd-h100'></textarea>
										</td>
									</tr>
									<tr>
										<th>(英)分类描述</th>
										<td>
											<textarea name="description_en" class='hd-w500 hd-h100'></textarea>
										</td>
									</tr>

									<tr>
										<th>
											分类排序
											<span class="star">*</span>
										</th>
										<td>
											<input name="sort" type="text" class='hd-w200' value="100" />
										</td>
									</tr>
									<tr>
										<th>
											是否显示
											<span class="star">*</span>
										</th>
										<td>
											<label>
												<input <?php if(!isset($parent['is_show'])||1 == $parent['is_show']): ?>checked="checked"<?php endif; ?> type="radio" name="is_show" value="1"/>
												<span>显示</span>
											</label>
											<label>
											<input <?php if(isset($parent['is_show'])&&0 == $parent['is_show']): ?>checked="checked"<?php endif; ?>type="radio" name="is_show" value="0"/>
												<span>不显示</span>
											</label>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div lab='view' class='hd-tab-area'>
							<table class='hd-table hd-table-form hd-form'>
								<tbody>
									<tr>
										<th class="hd-w100">
											每页记录数
											<span class="star">*</span>
										</th>
										<td>
											<input name="page" type="text" class='hd-w200' value="<?php if(isset($parent['page'])): echo ($parent["page"]); else: ?>20<?php endif; ?>" />
										</td>
									</tr>
									<tr>
										<th>
											栏目类型
											<span class="star">*</span>
										</th>
										<td>
											<label>
												<input <?php if(!isset($parent['cat_type'])||1 == $parent['cat_type']): ?>checked="checked"<?php endif; ?> type="radio" name="cat_type" value="1"/>
												<span>普通</span>
											</label>
											<label>
											<input <?php if(isset($parent['cat_type'])&&2 == $parent['cat_type']): ?>checked="checked"<?php endif; ?>type="radio" name="cat_type" value="2"/>
											<span>封面</span>
											</label>
											<label>
												<input <?php if(isset($parent['cat_type'])&&3 == $parent['cat_type']): ?>checked="checked"<?php endif; ?> type="radio" name="cat_type" value="3"/>
											<span>跳转</span>
											</label>
											<label>
												<input <?php if(isset($parent['cat_type'])&&4 == $parent['cat_type']): ?>checked="checked"<?php endif; ?>type="radio" name="cat_type" value="4"/>
											<span>单页</span>
											</label>
										</td>
									</tr>
									<tr>
										<th>跳转地址</th>
										<td>
											<input name="go_url" type="text" class='hd-w500' value="" />
										</td>
									</tr>
									<tr>
										<th>
											跳转到子栏目
											<span class="star">*</span>
										</th>
										<td>
											<label>
												<input <?php if(!isset($parent['go_child'])||0 == $parent['go_child']): ?>checked="checked"<?php endif; ?>
											type="radio" name="go_child" value="0"/>
											<span>否</span>
											</label>
											<label>
												<input <?php if(isset($parent['go_child'])&&1 == $parent['go_child']): ?>checked="checked"<?php endif; ?>
											type="radio" name="go_child" value="1"/>
											<span>是</span>
											</label>
										</td>
									</tr>
									<tr>
										<th>
											打开方式
											<span class="star">*</span>
										</th>
										<td>
											<label>
												<input <?php if(!isset($parent['target'])||1 == $parent['target']): ?>checked="checked"<?php endif; ?> type="radio" name="target" value="1"/>
												<span>当前窗口</span>
											</label>
											<label>
											<input <?php if(isset($parent['target'])&&2 == $parent['target']): ?>checked="checked"<?php endif; ?>type="radio" name="target" value="2"/>
												<span>新窗口</span>
											</label>
										</td>
									</tr>

									<tr>
										<th >
											封面模板
											<span class="star">*</span>
										</th>
										<td>
											<input  class='hd-w200' type="text" name='default_tpl' value="<?php if(isset($parent['default_tpl'])): echo ($parent["default_tpl"]); else: ?>default<?php endif; ?>"/>
									</td>
									</tr>
									<tr>
										<th>
											列表模板
											<span class="star">*</span>
										</th>
										<td>
											<input  class='hd-w200' type="text" name='list_tpl' value="<?php if(isset($parent['list_tpl'])): echo ($parent["list_tpl"]); else: ?>lists<?php endif; ?>"/>
									</td>
									</tr>
									<tr>
										<th>
											详细模板
											<span class="star">*</span>
										</th>
										<td>
											<input  class='hd-w200' type="text" name='view_tpl' value="<?php if(isset($parent['view_tpl'])): echo ($parent["view_tpl"]); else: ?>view<?php endif; ?>"/>
										</td>
									</tr>
									<tr>
										<th>
											控制器
											<span class="star">*</span>
										</th>
										<td>
											<input  class='hd-w200' type="text" name='remark' value="<?php if(isset($parent['remark'])): echo ($parent["remark"]); else: ?>Index<?php endif; ?>"/>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<input type="submit" class='hd-btn hd-btn-sm' value="添加" />
			<input type="button" class="hd-btn hd-btn-sm" onclick="location.href='<?php echo U('Category/index');?>'" value="返回"/>

	</form>
</body>
</html>