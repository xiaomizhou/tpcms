<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>模型管理</title>
    <script type='text/javascript' src='/dcms/Core/Org/Jquery/jquery-1.8.2.min.js'></script>
	<link href='/dcms/Core/Org/hdjs/hdjs.css' rel='stylesheet' media='screen'>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/hdjs.min.js'></script>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/org/cal/lhgcalendar.min.js'></script>
	<script type='text/javascript'>
		MODULE='/dcms/index.php/Admin'; //当前模块
		CONTROLLER='/dcms/index.php/Admin/Model'; //当前控制器)
		ACTION='/dcms/index.php/Admin/Model/index';//当前方法(方法)
		ROOT='/dcms'; //当前项目根路径
		PUBLIC= '/dcms/Core/Tpcms/Admin/View/Public';//当前定义的Public目录
	</script>
    <script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.base.js"></script><script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.model.js"></script>
    <link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.base.css" />
</head>
<body>
    <div class="hd-menu-list">
        <ul>
            <li class="active">
                <a href="javascript:;">模型列表</a>
            </li>
            <li>
                <a href="<?php echo U('Model/add');?>">添加模型</a>
            </li>
            <li>
                <a href="<?php echo U('Model/update_cache');?>" >更新缓存</a>
            </li>
        </ul>
    </div>
    <div class="hd-title-header">温馨提示</div>
    <div class="help">
        <ul>
            <li>
               请谨慎修改！
            </li>
        </ul>
    </div>
    <div class="content">
    		<form action = '<?php echo U("Model/beachdelete");?>' method='post' name="operationForm">
        <table class="hd-table hd-table-list hd-form">
            <thead>
                <tr>
                		<td class="hd-w30">
						<input type="checkbox" id="selectAllContent"/>
					</td>
                    <td class="hd-w30">mid</td>
                    <td>模型名称</td>
                    <td>表名称</td>
                    <td class="hd-w150">操作</td>
                </tr>
            </thead>
            <tbody>
                <?php if($data): if(is_array($data)): foreach($data as $key=>$v): ?><tr>
                		<td class="hd-w30">
							<input type="checkbox"  name="mid[<?php echo ($v["mid"]); ?>]" value="<?php echo ($v["mid"]); ?>" />
					</td>
                    <td><?php echo ($v["mid"]); ?></td>
                    <td><?php echo ($v["remark"]); ?></td>
                    <td><?php echo ($v["name"]); ?></td>
                    <td>
                        <a href="<?php echo U('ModelField/index',array('mid'=>$v['mid']));?>">字段管理</a>
                        |
                        <a href="<?php echo U('Model/edit',array('mid'=>$v['mid']));?>">修改</a>
                        |
                        <a href="javascript:;" onclick="del_modal('<?php echo U('Model/del',array('mid'=>$v['mid']));?>')">删除</a>
                    </td>
                </tr><?php endforeach; endif; ?>
                <?php else: ?>
                <tr>
                    <td colspan="4">没有找到符合条件的记录</td>
                </tr><?php endif; ?>
            </tbody>
        </table>
        <div class="hd-page"></div>
		<input type="button" class="hd-btn hd-btn-xm select_all"  value="全选" />
		<input type="button" class="hd-btn hd-btn-xm operation"  value="批量删除" name="update_del"/>
		</form>
    </div>
    

</body>
</html>