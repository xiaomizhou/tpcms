<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>添加文档</title>
	<script type='text/javascript' src='/dcms/Core/Org/Jquery/jquery-1.8.2.min.js'></script>
	<link href='/dcms/Core/Org/hdjs/hdjs.css' rel='stylesheet' media='screen'>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/hdjs.min.js'></script>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/org/cal/lhgcalendar.min.js'></script>
	<script type='text/javascript'>
		MODULE='/dcms/index.php/Admin'; //当前模块
		CONTROLLER='/dcms/index.php/Admin/Article'; //当前控制器)
		ACTION='/dcms/index.php/Admin/Article/add';//当前方法(方法)
		ROOT='/dcms'; //当前项目根路径
		PUBLIC= '/dcms/Core/Tpcms/Admin/View/Public';//当前定义的Public目录
	</script>
	<script type="text/javascript">
	$(function(){
		$('form').validate({<?php echo ($validate); ?>});
	})
	</script>
	<script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.base.js"></script><script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.article.js"></script>
	<link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.base.css" /><link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.article.css" />
</head>
<body>
	<form action="" method="post" enctype='multipart/form-data' class='hd-form' name="form">
		

			<?php if($curCatType != 4): ?><div class="hd-menu-list">
				<ul>
					<li>
						<a href="<?php echo U('Article/index',array('category_cid'=>$_GET['category_cid'],'verifystate'=>$_GET['verifystate']));?>" >文档列表</a>
					</li>
					<li class='active'>
						<a href="javascript:;" >添加文档</a>
					</li>
					
				</ul>
			</div><?php endif; ?>

			<div class="hd-title-header">温馨提示</div>
			<div class="help">
			    <ul>
			        <li>
			          <?php echo ($curCname); ?> 添加文档
			        </li>
			    </ul>
			</div>
			<div class="right_content">
				<div class="hd-tab">
					<ul class="hd-tab-menu">
						<li lab='base' class="active">
							<a href="#">基本信息</a>
						</li>
						<?php if($curCatType != 4): ?><li lab='pic'>
								<a href="#">图集信息</a>
							</li><?php endif; ?>
					</ul>
					<div class="hd-tab-content">
						<div lab='base' class="hd-table-area" >
							<table class='hd-table hd-table-form hd-form'>
								<tbody>
									<tr>
										<th class="hd-w100">
											栏目分类
											<span class="star">*</span>
										</th>
										<td>
											<?php if($curCatType != 4): ?><select name="category_cid" >
												<option value="0">请选择分类</option>
												<option value="<?php echo ($curCid); ?>" <?php if($curCid == $_GET['category_cid']): ?>selected='selected'<?php endif; ?>>
													<?php echo ($curCname); ?>
												</option>
												<?php if(is_array($topChild)): foreach($topChild as $key=>$v): ?><option value="<?php echo ($v["cid"]); ?>" <?php if($v['cid'] == $_GET['category_cid']): ?>selected='selected'<?php endif; ?>>---<?php echo ($v["_html"]); echo ($v["cname"]); ?></option><?php endforeach; endif; ?>
											</select>
											<?php else: ?>
												<?php echo ($curCname); ?>
												<input type="hidden" name="category_cid" value="<?php echo ($_GET['category_cid']); ?>" /><?php endif; ?>
										</td>
									</tr>
									<tr>
										<th>
											标题
											<span class="star">*</span>
										</th>
										<td>

											<input type="text" name="article_title" class='hd-w300' />	

										</td>
									</tr>
									<tr>
										<th>
											(英)标题
										</th>
										<td>

											<input type="text" name="article_title_en" class='hd-w300' />	

										</td>
									</tr>
									<tr>
										<th >
											关键字
											
										</th>
										<td>
											<textarea name="keywords" class='hd-w500 hd-h100'></textarea>
										</td>
									</tr>
									<tr>
										<th>
											(英)关键字
										</th>
										<td>
											<textarea name="keywords_en" class='hd-w500 hd-h100'></textarea>
										</td>
									</tr>
									<tr>
										<th>
										描述
										
										</th>
										<td>
											<textarea name="description" class='hd-w500 hd-h100'></textarea>
										</td>
									</tr>
									<tr>
										<th>
										(英)描述
										
										</th>
										<td>
											<textarea name="description_en" class='hd-w500 hd-h100'></textarea>
										</td>
									</tr>
									<tr >
										<th>
										排序
										<span class="star">*</span>
										</th>
										<td>
											<input type="text" name="sort" class='hd-w200' value="100" />	
										</td>
									</tr>

									<tr>
										<th>
										浏览次数
										<span class="star">*</span>
										</th>
										<td>
											<input type="text" name="click" class='hd-w200' value="100" />	
										</td>
									</tr>

									<tr>
										<th>
										发布时间
										<span class="star">*</span>
										</th>
										<td>
											<input id='addtime' type="text" name="addtime" class='hd-w200' />	
											<script>
												$('#addtime').calendar({
													format : 'yyyy-MM-dd'
												});
											</script>
										</td>
									</tr>

									<tr>
										<th>
											审核
											<span class="star">*</span>
										</th>
										<td>
											<label >
												<input name="verifystate" type="radio"  value="1" />
												审核中
											</label>&nbsp;&nbsp;
											<label >
												<input checked="checked" name="verifystate" type="radio" value="2" />
												通过
											</label>
										</td>
									</tr>
									<tr>
										<th>
											属性
											
										</th>
										<td>
											<?php if(is_array($flag)): foreach($flag as $key=>$v): ?><label >
													<input name="flag[]" type="checkbox"  value="<?php echo ($v); ?>" />
													<?php echo ($v); ?>(<?php echo ($key); ?>)
												</label>&nbsp;&nbsp;<?php endforeach; endif; ?>
										</td>
									</tr>
								
									<tr >
										<th>文件</th>
										<td>
											<input type="file" name='file'/>
										</td>
									</tr>
									<tr>
										<th>
											图片
											
										</th>
										<td>
											<input type="file" name='pic'/>
										</td>
									</tr>
									
									<?php if(is_array($extForm)): foreach($extForm as $key=>$v): ?><tr>
											<th><?php echo ($v["title"]); ?></th>
											<td><?php echo ($v["html"]); ?></td>
										</tr><?php endforeach; endif; ?>

									<?php if($attrForm): ?><tr><th colspan='2' style='text-align:left'>筛选</th></tr>
										<?php if(is_array($attrForm)): foreach($attrForm as $key=>$v): ?><tr <?php if($v["is_pic"]): ?>pic='1'<?php else: ?>pic='0'<?php endif; ?>>
											<th><?php echo ($v["title"]); ?></th>
											<td><?php echo ($v["html"]); ?></td>
										</tr><?php endforeach; endif; endif; ?>
									
								

								</tbody>
							  </table>
							  
							 
							   
						</div>
						<div lab='pic' class='hd-tab-area' id='pic'>

							<div class="no_pic">
							<table class='hd-table hd-table-form hd-form'>
								<tbody>
				
									<tr>
										<th class="hd-w100"><span class="hand" onclick="add_pic(this)">[+]</span></th>
										<td ><input type="file" name="img[]" class="input noborder"></td>
									</tr>
									
								</tbody>
							</table>
							</div>
							<div class="has_pic"  style="display:none">
								<div class="hd-tab">
					                <ul class="hd-tab-menu">
					                   
					                    
					                </ul>
            
						            <div class="hd-tab-content">
						        
							        </div>
					            </div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		
			<input type="hidden" name="isattr" value="0" id='isattr' />
			<input type="submit" class='hd-btn hd-btn-sm' value="添加" />
			<input type="button" class="hd-btn hd-btn-sm" onclick="location.href='<?php echo U('Article/index',array('category_cid'=>$_GET['category_cid']));?>'" value="返回"/>

	</form>
</body>
</html>