<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>广告位置管理</title>
    <script type='text/javascript' src='/dcms/Core/Org/Jquery/jquery-1.8.2.min.js'></script>
	<link href='/dcms/Core/Org/hdjs/hdjs.css' rel='stylesheet' media='screen'>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/hdjs.min.js'></script>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/org/cal/lhgcalendar.min.js'></script>
	<script type='text/javascript'>
		MODULE='/dcms/index.php/Admin'; //当前模块
		CONTROLLER='/dcms/index.php/Admin/Position'; //当前控制器)
		ACTION='/dcms/index.php/Admin/Position/index';//当前方法(方法)
		ROOT='/dcms'; //当前项目根路径
		PUBLIC= '/dcms/Core/Tpcms/Admin/View/Public';//当前定义的Public目录
	</script>
    <script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.base.js"></script><script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.position.js"></script>
    <link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.base.css" />
</head>
<body>
    <div class="hd-menu-list">
        <ul>
            <li class="active">
                <a href="javascript:;">广告位置列表</a>
            </li>
            <li>
                <a href="<?php echo U('Position/add');?>">添加广告位置</a>
            </li>
            <li>
                <a href="<?php echo U('Position/update_cache');?>" >更新缓存</a>
            </li>
        </ul>
    </div>
    <div class="content">
        <table class="hd-table hd-table-list hd-form">
            <thead>
                <tr>
                    <td class="hd-w30">psid</td>
                    <td>广告位置名称</td>
                    <td>宽度</td>
                    <td>高度</td>
                    <td class="hd-w150">操作</td>
                </tr>
            </thead>
            <tbody>
                <?php if($data): if(is_array($data)): foreach($data as $key=>$v): ?><tr>
                    <td><?php echo ($v["psid"]); ?></td>
                    <td><?php echo ($v["position_name"]); ?></td>
                    <td><?php echo ($v["width"]); ?>px</td>
                    <td><?php echo ($v["height"]); ?>px</td>
                    <td>
                       
                        <a href="<?php echo U('Position/edit',array('psid'=>$v['psid']));?>">修改</a>
                        |
                        <a href="javascript:;" onclick="del_modal('<?php echo U('Position/del',array('psid'=>$v['psid']));?>')">删除</a>
                    </td>
                </tr><?php endforeach; endif; ?>
                <?php else: ?>
                <tr>
                    <td colspan="4">没有找到符合条件的记录</td>
                </tr><?php endif; ?>
            </tbody>
        </table>
    </div>
    

</body>
</html>