<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>添加类型</title>
    <script type='text/javascript' src='/dcms/Core/Org/Jquery/jquery-1.8.2.min.js'></script>
	<link href='/dcms/Core/Org/hdjs/hdjs.css' rel='stylesheet' media='screen'>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/hdjs.min.js'></script>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/org/cal/lhgcalendar.min.js'></script>
	<script type='text/javascript'>
		MODULE='/dcms/index.php/Admin'; //当前模块
		CONTROLLER='/dcms/index.php/Admin/Type'; //当前控制器)
		ACTION='/dcms/index.php/Admin/Type/add';//当前方法(方法)
		ROOT='/dcms'; //当前项目根路径
		PUBLIC= '/dcms/Core/Tpcms/Admin/View/Public';//当前定义的Public目录
	</script>
    <script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.base.js"></script><script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.type.js"></script>
    <link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.base.css" />
</head>
<body>
    <form action="" method="post" class="hd-form" name="form" >
        <div class="hd-menu-list">
            <ul>
                <li >
                    <a href="<?php echo U('Type/index');?>">类型列表</a>
                </li>
                <li class="active">
                    <a href="javascript:;">添加类型</a>
                </li>
            </ul>
        </div>
        <div class="hd-title-header">温馨提示</div>
        <div class="help">
            <ul>
                <li>
                   请谨慎修改！
                </li>
            </ul>
        </div>
        <div class="hd-title-header">添加类型</div>
        <div class="right_content">
            <table class="hd-table hd-table-form">
                <tbody>
                    <tr>
                        <th class="hd-w100">
                            类型名称
                            <span class="star">*</span>
                        </th>
                        <td>
                            <input type="text" name="typename" class="hd-w200"></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <input type="submit" value="添加" class="hd-btn hd-btn-sm">
        <input type="button" value="返回" class="hd-btn hd-btn-sm" onclick="location.href='<?php echo U('Type/index');?>'">
        </form>
</body>
</html>