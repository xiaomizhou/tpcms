<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>编辑栏目</title>
	<script type='text/javascript' src='/dcms/Core/Org/Jquery/jquery-1.8.2.min.js'></script>
	<link href='/dcms/Core/Org/hdjs/hdjs.css' rel='stylesheet' media='screen'>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/hdjs.min.js'></script>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/org/cal/lhgcalendar.min.js'></script>
	<script type='text/javascript'>
		MODULE='/dcms/index.php/Admin'; //当前模块
		CONTROLLER='/dcms/index.php/Admin/Category'; //当前控制器)
		ACTION='/dcms/index.php/Admin/Category/edit';//当前方法(方法)
		ROOT='/dcms'; //当前项目根路径
		PUBLIC= '/dcms/Core/Tpcms/Admin/View/Public';//当前定义的Public目录
	</script>
	<script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.base.js"></script><script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.category.js"></script>
	<link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.base.css" />
</head>
<body>
	<form action="" method="post" enctype='multipart/form-data' class='hd-form' name="form">
		
			<div class="hd-menu-list">
				<ul>
					<li>
						<a href="<?php echo U('Category/index');?>" >栏目列表</a>
					</li>
					<li class='active'>
						<a href="javascript:;" >编辑栏目</a>
					</li>
					
				</ul>
			</div>

			<div class="right_content">
				<div class="hd-tab">
					<ul class="hd-tab-menu">
						<li lab='base' class="active">
							<a href="#">基本配置</a>
						</li>
						<li lab='view'>
							<a href="#">模板配置</a>
						</li>
					</ul>
					<div class="hd-tab-content">
						<div lab='base' class='hd-table-area'>
							<table class='hd-table hd-table-form hd-form'>
								<tbody>
									<tr>
										<th class="hd-w100">
											模型
											<span class="star">*</span>
										</th>
										<td>

											<?php echo ($data["model"]); ?>
										</td>
									</tr>
									<tr >
										<th>
											文档类型
										
										</th>
										<td>

											<select name="type_typeid">
												<option value="0">请选择文档类型</option>
												<?php if(is_array($type)): foreach($type as $key=>$v): ?><option  value="<?php echo ($v["typeid"]); ?>" <?php if( $v['typeid'] == $data['type_typeid']): ?>selected="selected"<?php endif; ?>
													><?php echo ($v["typename"]); ?>
												</option><?php endforeach; endif; ?>
										</select>

									</td>
									</tr>
									<tr>
										<th >
											上级分类
											
										</th>
										<td>
											<?php echo ($topCname); ?>
										</td>
									</tr>
									<tr>
										<th >
										分类名称
										<span class="star">*</span>
										</th>
										<td>
											<input type="text" name="cname"  class='hd-w200' value="<?php echo ($data["cname"]); ?>" />
										</td>
									</tr>
									<tr>
										<th >
										(英)分类名称
										<span class="star">*</span>
										</th>
										<td>
											<input type="text" name="cname_en"  class='hd-w200' value="<?php echo ($data["cname_en"]); ?>" />
										</td>
									</tr>

									<tr >
										<th>栏目图片</th>
										<td>
											<input type="file" name='pic'/>
										
											
											<?php if($data["has_pic"]): ?><br /><br />
												<img src='<?php echo ($data["pathpic"]); ?>' width="100" />
												<a href="javascript:;" onclick = "ajax_del_attachment(this,'<?php echo ($data["cid"]); ?>','pic')">删除</a><?php endif; ?>
										</td>
									</tr>
									<tr>
										<th>分类关键字</th>
										<td>
											<textarea name="keywords" class='hd-w500 hd-h100'><?php echo ($data["keywords"]); ?></textarea>
										</td>
									</tr>
									<tr>
										<th>(英)分类关键字</th>
										<td>
											<textarea name="keywords_en" class='hd-w500 hd-h100'><?php echo ($data["keywords_en"]); ?></textarea>
										</td>
									</tr>
									<tr>
										<th>分类描述</th>
										<td>
											<textarea name="description" class='hd-w500 hd-h100'><?php echo ($data["description"]); ?></textarea>
										</td>
									</tr>
									<tr>
										<th>(英)分类描述</th>
										<td>
											<textarea name="description_en" class='hd-w500 hd-h100'><?php echo ($data["description_en"]); ?></textarea>
										</td>
									</tr>
									<tr>
										<th>
											分类排序
											<span class="star">*</span>
										</th>
										<td>
											<input name="sort" type="text" class='hd-w200' value="<?php echo ($data["sort"]); ?>" />
										</td>
									</tr>
									<tr>
										<th>
											是否显示
											<span class="star">*</span>
										</th>
										<td>
											<label>
												<input <?php if(1 == $data['is_show']): ?>checked="checked"<?php endif; ?> type="radio" name="is_show" value="1"/>
												<span>显示</span>
											</label>
											<label>
											<input <?php if(0 == $data['is_show']): ?>checked="checked"<?php endif; ?>type="radio" name="is_show" value="0"/>
												<span>不显示</span>
											</label>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div lab='view' class='hd-tab-area'>
							<table class='hd-table hd-table-form hd-form'>
								<tbody>
									<tr>
										<th class="hd-w100">
											每页记录数
											<span class="star">*</span>
										</th>
										<td>
											<input name="page" type="text" class='hd-w200' value="<?php echo ($data["page"]); ?>" />
										</td>
									</tr>
									<tr>
										<th>
											栏目类型
											<span class="star">*</span>
										</th>
										<td>
											<label>
												<input <?php if(1 == $data['cat_type']): ?>checked="checked"<?php endif; ?> type="radio" name="cat_type" value="1"/>
												<span>普通</span>
											</label>
											<label>
											<input <?php if(2 == $data['cat_type']): ?>checked="checked"<?php endif; ?>type="radio" name="cat_type" value="2"/>
											<span>封面</span>
											</label>
											<label>
												<input <?php if( 3 == $data['cat_type']): ?>checked="checked"<?php endif; ?> type="radio" name="cat_type" value="3"/>
											<span>跳转</span>
											</label>
											<label>
												<input <?php if(4 == $data['cat_type']): ?>checked="checked"<?php endif; ?>type="radio" name="cat_type" value="4"/>
											<span>单页</span>
											</label>
										</td>
									</tr>
									<tr>
										<th>跳转地址</th>
										<td>
											<input name="go_url" type="text" class='hd-w500' value="<?php echo ($data["go_url"]); ?>"  />
										</td>
									</tr>
									<tr>
										<th>
											跳转到子栏目
											<span class="star">*</span>
										</th>
										<td>
											<label>
												<input <?php if(0 == $data['go_child']): ?>checked="checked"<?php endif; ?>
											type="radio" name="go_child" value="0"/>
											<span>否</span>
											</label>
											<label>
												<input <?php if(1 == $data['go_child']): ?>checked="checked"<?php endif; ?>
											type="radio" name="go_child" value="1"/>
											<span>是</span>
											</label>
										</td>
									</tr>
									<tr>
										<th>
											打开方式
											<span class="star">*</span>
										</th>
										<td>
											<label>
												<input <?php if(1 == $data['target']): ?>checked="checked"<?php endif; ?> type="radio" name="target" value="1"/>
												<span>当前窗口</span>
											</label>
											<label>
											<input <?php if(2 == $data['target']): ?>checked="checked"<?php endif; ?>type="radio" name="target" value="2"/>
												<span>新窗口</span>
											</label>
										</td>
									</tr>

									<tr>
										<th >
											封面模板
											<span class="star">*</span>
										</th>
										<td>
											<input  class='hd-w200' type="text" name='default_tpl' value="<?php echo ($data["default_tpl"]); ?>"/>
									</td>
									</tr>
									<tr>
										<th>
											列表模板
											<span class="star">*</span>
										</th>
										<td>
											<input  class='hd-w200' type="text" name='list_tpl' value="<?php echo ($data["list_tpl"]); ?>"/>
									</td>
									</tr>
									<tr>
										<th>
											详细模板
											<span class="star">*</span>
										</th>
										<td>
											<input  class='hd-w200' type="text" name='view_tpl' value="<?php echo ($data["view_tpl"]); ?>"/>
										</td>
									</tr>
									<tr>
										<th>
											控制器
											<span class="star">*</span>
										</th>
										<td>
											<input  class='hd-w200' type="text" name='remark' value="<?php echo ($data["remark"]); ?>"/>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" name='cid' value="<?php echo ($_GET['cid']); ?>" />
			<input type="submit" class='hd-btn hd-btn-sm' value="编辑" />
			<input type="button" class="hd-btn hd-btn-sm" onclick="location.href='<?php echo U('Category/index');?>'" value="返回"/>

	</form>
<script type="text/javascript">
	var ajaxDelAttachmentUrl = "<?php echo U('Category/ajax_del_attachment');?>";
</script>
</body>
</html>