<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>编辑文档</title>
	<script type='text/javascript' src='/dcms/Core/Org/Jquery/jquery-1.8.2.min.js'></script>
	<link href='/dcms/Core/Org/hdjs/hdjs.css' rel='stylesheet' media='screen'>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/hdjs.min.js'></script>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/org/cal/lhgcalendar.min.js'></script>
	<script type='text/javascript'>
		MODULE='/dcms/index.php/Admin'; //当前模块
		CONTROLLER='/dcms/index.php/Admin/Article'; //当前控制器)
		ACTION='/dcms/index.php/Admin/Article/edit';//当前方法(方法)
		ROOT='/dcms'; //当前项目根路径
		PUBLIC= '/dcms/Core/Tpcms/Admin/View/Public';//当前定义的Public目录
	</script>
	<script type="text/javascript">
	$(function(){
		$('form').validate({<?php echo ($validate); ?>});
	})
	</script>
	<script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.base.js"></script><script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.article.js"></script>
	<link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.base.css" /><link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.article.css" />
	<script type='text/javascript'>
		// 删除图集
		var ajaxDelPicUrl = "<?php echo U('article/ajax_del_pic');?>";
		var ajaxDelAttrPicUrl= "<?php echo U('article/ajax_attr_del_pic');?>";
		var aid = "<?php echo ($_GET['aid']); ?>";
		// 删除附件
		var ajaxDelAttachmentUrl = "<?php echo U('article/ajax_del_attachment');?>";
	</script>
</head>
<body>
	<form action="" method="post" enctype='multipart/form-data' class='hd-form' name="form">
		

			<?php if($curCatType != 4): ?><div class="hd-menu-list">
				<ul>
					<li>
						<a href="<?php echo U('Article/index',array('category_cid'=>$_GET['category_cid'],'verifystate'=>$_GET['verifystate']));?>" >文档列表</a>
					</li>
					<li class='active'>
						<a href="javascript:;" >编辑文档</a>
					</li>
					
				</ul>
			</div><?php endif; ?>

			<div class="hd-title-header">温馨提示</div>
			<div class="help">
			    <ul>
			        <li>
			          <?php echo ($curCname); ?> 编辑文档
			        </li>
			    </ul>
			</div>
			<div class="right_content">
				<div class="hd-tab">
					<ul class="hd-tab-menu">
						<li lab='base' class="active">
							<a href="#">基本信息</a>
						</li>
						<?php if($curCatType != 4): ?><li lab='pic'>
								<a href="#">图集信息</a>
							</li><?php endif; ?>
					</ul>
					<div class="hd-tab-content">
						<div lab='base' class='hd-table-area'>
							<table class='hd-table hd-table-form hd-form'>
								<tbody>
									<tr>
										<th class="hd-w100">
											栏目分类
											<span class="star">*</span>
										</th>
										<td>
											<?php if($curCatType != 4): ?><select name="category_cid" >
												<option value="0">请选择分类</option>
												<option value="<?php echo ($curCid); ?>" <?php if($curCid == $_GET['category_cid']): ?>selected='selected'<?php endif; ?>>
													<?php echo ($curCname); ?>
												</option>
												<?php if(is_array($topChild)): foreach($topChild as $key=>$v): ?><option value="<?php echo ($v["cid"]); ?>" <?php if($v['category_cid'] == $_GET['category_cid']): ?>selected='selected'<?php endif; ?>>---<?php echo ($v["_html"]); echo ($v["cname"]); ?></option><?php endforeach; endif; ?>
											</select>
											<?php else: ?>
												<?php echo ($curCname); ?>
													<input type="hidden" name="category_cid" value="<?php echo ($_GET['category_cid']); ?>" /><?php endif; ?>
										</td>
									</tr>
									<tr>
										<th>
											标题
											<span class="star">*</span>
										</th>
										<td>

											<input type="text" name="article_title" class='hd-w300' value="<?php echo ($data["article_title"]); ?>" />	

										</td>
									</tr>
									<tr>
										<th>
											(英)标题
											<span class="star">*</span>
										</th>
										<td>

											<input type="text" name="article_title_en" class='hd-w300' value="<?php echo ($data["article_title_en"]); ?>" />	

										</td>
									</tr>
									<tr>
										<th >
											关键字
											
										</th>
										<td>
											<textarea name="keywords" class='hd-w500 hd-h100'><?php echo ($data["keywords"]); ?></textarea>
										</td>
									</tr>
									<tr>
										<th >
											(英)关键字
											
										</th>
										<td>
											<textarea name="keywords_en" class='hd-w500 hd-h100'><?php echo ($data["keywords_en"]); ?></textarea>
										</td>
									</tr>
									<tr>
										<th>
										描述
										
										</th>
										<td>
											<textarea name="description" class='hd-w500 hd-h100'><?php echo ($data["description"]); ?></textarea>
										</td>
									</tr>
									<tr>
										<th>
										(英)描述
										</th>
										<td>
											<textarea name="description_en" class='hd-w500 hd-h100'><?php echo ($data["description_en"]); ?></textarea>
										</td>
									</tr>
									<tr>
										<th>
										排序
										<span class="star">*</span>
										</th>
										<td>
											<input type="text" name="sort" class='hd-w200' value="100" value="<?php echo ($data["sort"]); ?>" />	
										</td>
									</tr>

									<tr>
										<th>
										浏览次数
										<span class="star">*</span>
										</th>
										<td>
											<input type="text" name="click" class='hd-w200' value="100" value="<?php echo ($data["click"]); ?>" />	
										</td>
									</tr>

									<tr>
										<th>
										发布时间
										<span class="star">*</span>
										</th>
										<td>
											<input id='addtime' type="text" name="addtime" class='hd-w200' value="<?php echo (date('Y-m-d',$data["addtime"])); ?>" />	
											<script>
												$('#addtime').calendar({
													format : 'yyyy-MM-dd'
												});
											</script>
										</td>
									</tr>

									<tr>
										<th>
											审核
											<span class="star">*</span>
										</th>
										<td>
											<label >
												<input name="verifystate" type="radio"  value="1" <?php if($data["verifystate"] == 1): ?>checked='checked'<?php endif; ?> />
												审核中
											</label>&nbsp;&nbsp;
											<label >
												<input  name="verifystate" type="radio" value="2" <?php if($data["verifystate"] == 2): ?>checked='checked'<?php endif; ?>/>
												通过
											</label>
										</td>
									</tr>
									<tr>
										<th>
											属性
											
										</th>
										<td>
											<?php if(is_array($flag)): foreach($flag as $key=>$v): ?><label >
													<input <?php if(strchr($data['flag'],$v)): ?>checked='checked'<?php endif; ?> name="flag[]" type="checkbox"  value="<?php echo ($v); ?>" />
													<?php echo ($v); ?>(<?php echo ($key); ?>)
												</label>&nbsp;&nbsp;<?php endforeach; endif; ?>
										</td>
									</tr>
									
									<tr >
										<th>文件</th>
										<td>
											<input type="file" name='file'/>
											
											<?php if($data['file']): ?><br/><br/>
												<a href="<?php echo U('down',array('aid'=>$data['aid'],'field'=>'file'));?>">下载</a>
											
												<a href="javascript:;" onclick="ajax_del_attachment(this,'<?php echo ($data["aid"]); ?>','file')">删除</a><?php endif; ?>
										</td>
									</tr>
									<tr>
										<th>
											图片
											
										</th>
										<td>
											<input type="file" name='pic'/>
											
											<?php if($data["pic"]): ?><br/><br/>
												<img src="/dcms/<?php echo ($data["pic"]); ?>" alt=""  width="100" />
												<a href="javascript:;" onclick="ajax_del_attachment(this,'<?php echo ($data["aid"]); ?>','pic')">删除</a><?php endif; ?>
										</td>
									</tr>
									
									<?php if(is_array($extForm)): foreach($extForm as $key=>$v): ?><tr>
											<th><?php echo ($v["title"]); ?></th>
											<td><?php echo ($v["html"]); ?></td>
										</tr><?php endforeach; endif; ?>


									
									<?php if($attrForm): ?><tr><th colspan='2' style='text-align:left'>筛选</th></tr>
										<?php if(is_array($attrForm)): foreach($attrForm as $key=>$v): ?><tr <?php if($v["is_pic"]): ?>pic='1'<?php else: ?>pic='0'<?php endif; ?>>
											<th><?php echo ($v["title"]); ?></th>
											<td><?php echo ($v["html"]); ?></td>
										</tr><?php endforeach; endif; endif; ?>
								</tbody>
							</table>
							 
						</div>
						<div lab='pic' class='hd-tab-area' id='pic'>
							
							<div class="no_pic" <?php if($isattr): ?>style='display:none'<?php endif; ?>>
							
							<ul class='one_pic'>
								<?php if(is_array($pics)): foreach($pics as $key=>$p): ?><li>
										<img src="<?php echo ($p["big"]); ?>" alt=""  width="100" />
									
										<span aid="<?php echo ($p["article_aid"]); ?>" id="<?php echo ($p["id"]); ?>" onclick='del_img(this)'>×</span>
									</li><?php endforeach; endif; ?>
							</ul>


							<table class='hd-table hd-table-form hd-form'>
								<tbody>

									<tr>
										<th class="hd-w100"><span class="hand" onclick="add_pic(this)">[+]</span></th>
										<td ><input type="file" name="img[]" class="input noborder"></td>
									</tr>
									
								</tbody>
							</table>
							</div>

							<div class="has_pic"  <?php if(!$isattr): ?>style='display:none'<?php endif; ?>>
							
								<div class="hd-tab">
					                <ul class="hd-tab-menu">
					                  <?php $k==0; ?>
					                  <?php if(is_array($pics)): $i = 0; $__LIST__ = $pics;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$attr): $mod = ($i % 2 );++$i;?><li lab='<?php echo ($attr["attr_value_name"]); ?>' <?php if(!$k): ?>class='active'<?php endif; ?>>
											<a href="#"><?php echo ($attr["attr_value_name"]); ?></a>
										</li>
										<?php $k++; endforeach; endif; else: echo "" ;endif; ?>
					                </ul>
					                <div class="hd-tab-content ">

					                <?php $k==0; ?>
            						<?php if(is_array($pics)): $i = 0; $__LIST__ = $pics;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$attr): $mod = ($i % 2 );++$i;?><div lab='<?php echo ($attr["attr_value_name"]); ?>' class='hd-tab-area' >
											<ul class='one_pic'>
								
												<?php if(is_array($attr["pics"])): foreach($attr["pics"] as $key=>$p): ?><li>
													<img src="<?php echo ($p["big"]); ?>" alt=""  width="100" />
												
													<span aid="<?php echo ($p["article_aid"]); ?>" id="<?php echo ($p["id"]); ?>" onclick='del_img(this)'>×</span>
												</li><?php endforeach; endif; ?>
											
											</ul>
											<table class="hd-table hd-table-form hd-form">
			                                    <tbody>
			                                        <tr>
			                                            <th class="hd-w100"><span class="hand" onclick="add_pic(this)">[+]</span></th>
			                                            <td ><input type="file" name="<?php echo ($attr["attr_value_attr_value_id"]); ?>[]" class="input noborder"></td>
			                                        </tr>
			                                    </tbody>
			                                </table>


								      	</div>
							            <?php $k++; endforeach; endif; else: echo "" ;endif; ?>
							          </div>
					            </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" name="isattr" value="<?php echo ($isattr); ?>" id='isattr' />
			<input type="hidden" name="aid" value="<?php echo ($data['aid']); ?>" />
			<input type="submit" class='hd-btn hd-btn-sm' value="编辑" />
			<?php if($curCatType != 4): ?><input type="button" class="hd-btn hd-btn-sm" onclick="location.href='<?php echo U('Article/index',array('category_cid'=>$_GET['category_cid']));?>'" value="返回"/><?php endif; ?>

	</form>
</body>
</html>