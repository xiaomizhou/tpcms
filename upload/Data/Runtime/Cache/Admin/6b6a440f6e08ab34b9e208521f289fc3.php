<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>添加广告</title>
    <script type='text/javascript' src='/dcms/Core/Org/Jquery/jquery-1.8.2.min.js'></script>
	<link href='/dcms/Core/Org/hdjs/hdjs.css' rel='stylesheet' media='screen'>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/hdjs.min.js'></script>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/org/cal/lhgcalendar.min.js'></script>
	<script type='text/javascript'>
		MODULE='/dcms/index.php/Admin'; //当前模块
		CONTROLLER='/dcms/index.php/Admin/Ad'; //当前控制器)
		ACTION='/dcms/index.php/Admin/Ad/add';//当前方法(方法)
		ROOT='/dcms'; //当前项目根路径
		PUBLIC= '/dcms/Core/Tpcms/Admin/View/Public';//当前定义的Public目录
	</script>
    <script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.base.js"></script><script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.ad.js"></script>
    <link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.base.css" />
</head>
<body>
    <form action="" method="post" class="hd-form" name="form" enctype='multipart/form-data' >
        <div class="hd-menu-list">
            <ul>
                <li >
                    <a href="<?php echo U('Ad/index',array('verifystate'=>$_GET['verifystate']));?>">广告列表</a>
                </li>
                <li class="active">
                    <a href="javascript:;">添加广告</a>
                </li>
            </ul>
        </div>
        <div class="hd-title-header">添加广告</div>
        <div class="right_content">
            <table class="hd-table hd-table-form" >
                <tbody>
                    <tr>
                        <th class="hd-w100">
                            广告位置
                            <span class="star">*</span>
                        </th>
                        <td>
                            <select name="position_psid" >
                                <option value="0">请选择广告位置</option>
                                <?php if(is_array($position)): foreach($position as $key=>$v): ?><option value="<?php echo ($v["psid"]); ?>" <?php if($v["psid"] == $_GET["psid"]): ?>selected='selected'<?php endif; ?>><?php echo ($v["position_name"]); ?> <?php if($v["width"] && $v["height"]): ?>[<?php echo ($v["width"]); ?>×<?php echo ($v["height"]); ?>]<?php endif; ?></option><?php endforeach; endif; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th class="hd-w100">
                            广告名称
                            <span class="star">*</span>
                        </th>
                        <td>
                            <input type="text" name="name" class="hd-w200">
                        </td>
                    </tr>
                    <tr>
                        <th class="hd-w100">
                            (英)广告名称
                        </th>
                        <td>
                            <input type="text" name="name_en" class="hd-w200">
                        </td>
                    </tr>
               <!--      <tr>
                        <th class="hd-w100">
                            说明
                            <span class="star">*</span>
                        </th>
                        <td>
                           <textarea name="info" class="hd-w500 hd-h50"></textarea>
                        </td>
                    </tr> -->
                    <tr>
                        <th class="hd-w100">
                            排序
                            <span class="star">*</span>
                        </th>
                        <td>
                            <input type="text" name="sort" class="hd-w200" value='100' />
                        </td>
                    </tr>
                    <tr>
                        <th>
                            审核
                            <span class="star">*</span>
                        </th>
                        <td>
                            <label >
                                <input name="verifystate" type="radio"  value="1" />
                                审核中
                            </label>&nbsp;&nbsp;
                            <label >
                                <input checked="checked" name="verifystate" type="radio" value="2" />
                                通过
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th class="hd-w100">
                            广告链接
                        </th>
                        <td>
                            <textarea name="url" class='hd-w500 hd-h50'></textarea>
                        </td>
                    </tr>
                    
                    <tr>
                        <th class="hd-w100">
                            广告图片
                        </th>
                        <td>
                            <input type="file" name="pic" />
                        </td>
                    </tr>
                     <tr>
                        <th class="hd-w100">
                            (英)广告图片
                        </th>
                        <td>
                            <input type="file" name="pic_en" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <input type="submit" value="添加" class="hd-btn hd-btn-sm">
        <input type="button" value="返回" class="hd-btn hd-btn-sm" onclick="location.href='<?php echo U('Model/index');?>'">
        </form>

</body>
</html>