<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>编辑<?php echo ($name); ?>基本信息</title>
    <script type='text/javascript' src='/dcms/Core/Org/Jquery/jquery-1.8.2.min.js'></script>
	<link href='/dcms/Core/Org/hdjs/hdjs.css' rel='stylesheet' media='screen'>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/hdjs.min.js'></script>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/org/cal/lhgcalendar.min.js'></script>
	<script type='text/javascript'>
		MODULE='/dcms/index.php/Admin'; //当前模块
		CONTROLLER='/dcms/index.php/Admin/User'; //当前控制器)
		ACTION='/dcms/index.php/Admin/User/info';//当前方法(方法)
		ROOT='/dcms'; //当前项目根路径
		PUBLIC= '/dcms/Core/Tpcms/Admin/View/Public';//当前定义的Public目录
	</script>
    <script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.base.js"></script><script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.user.js"></script>
    <link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.base.css" />
</head>
<body>
    <form action="" method="post" class="hd-form" name="form" >
      
        <div class="hd-menu-list">
            <ul>
             
                <li class="active">
                    <a href="javascript:;">编辑管理员信息</a>
                </li>
            </ul>
        </div>
         <div class="hd-title-header">温馨提示</div>
        <div class="help">
            <ul>
                <li>
                  编辑管理员信息
                </li>
            </ul>
        </div>
        <div class="hd-title-header">编辑管理员信息</div>
        <div class="right_content">
            <table class="hd-table hd-table-form">
                <tbody>
                    <tr>
                        <th class="hd-w100">
                            用户名
                            <span class="star">*</span>
                        </th>
                        <td>
                      	<?php echo (session('user_name')); ?>

                        </td>
                    </tr>

                    <tr>
                        <th class="hd-w100">
                            昵称
                            <span class="star">*</span>
                        </th>
                        <td>
                            <input type="text" name="nickname" class="hd-w200"  value="<?php echo ($data["nickname"]); ?>"></td>
                    </tr>
                    <tr>
                        <th class="hd-w100">
                            邮箱
                            <span class="star">*</span>
                        </th>
                        <td>
                            <input type="text" name="email" class="hd-w200" value="<?php echo ($data["email"]); ?>"></td>
                    </tr>
                  
                </tbody>
            </table>
        </div>

        <input type="hidden" name="uid" value="<?php echo ($data["uid"]); ?>" />
        <input type="submit" value="编辑" class="hd-btn hd-btn-sm">
  
        </form>

</body>
</html>