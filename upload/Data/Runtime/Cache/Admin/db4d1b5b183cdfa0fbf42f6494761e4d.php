<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>留言列表</title>
	<script type='text/javascript' src='/dcms/Core/Org/Jquery/jquery-1.8.2.min.js'></script>
	<link href='/dcms/Core/Org/hdjs/hdjs.css' rel='stylesheet' media='screen'>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/hdjs.min.js'></script>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/org/cal/lhgcalendar.min.js'></script>
	<script type='text/javascript'>
		MODULE='/dcms/index.php/Admin'; //当前模块
		CONTROLLER='/dcms/index.php/Admin/Feedback'; //当前控制器)
		ACTION='/dcms/index.php/Admin/Feedback/index';//当前方法(方法)
		ROOT='/dcms'; //当前项目根路径
		PUBLIC= '/dcms/Core/Tpcms/Admin/View/Public';//当前定义的Public目录
	</script>
	<script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.base.js"></script>
	<link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/Css/mod.base.css" />
</head>
<body>

	<div class="hd-title-header">温馨提示</div>
	<div class="help">
	    <ul>
	        <li>
	          留言列表
	        </li>
	    </ul>
	</div>
	<form action="<?php echo U('Feedback/beachdelete');?>" method="post" name="operationForm">

		<table class="hd-table hd-table-list hd-form">
			<thead>
				<tr>
					<td class="hd-w30">
						<input type="checkbox" />
					</td>
					<td class="hd-w30">FDID</td>
					<td>姓名</td>
					<td>手机</td>
					<td>邮箱</td>
					<td>留言时间</td>
					<td>状态</td>
					<td class="hd-w180">操作</td>
				</tr>
			</thead>
			<?php if($data): if(is_array($data)): foreach($data as $key=>$v): ?><tr <?php if($v['lookstate'] == 1): ?>class='strong'<?php endif; ?>
					>
					<td>
						<input type="checkbox" name="fd_id[<?php echo ($v["fd_id"]); ?>]" value="<?php echo ($v["fd_id"]); ?>"/>
					</td>
					<td><?php echo ($v["fd_id"]); ?></td>
					<td><?php echo ($v["people"]); ?></td>
					<td><?php echo ($v["phone"]); ?></td>
					<td><?php echo ($v["email"]); ?></td>
					<td><?php if($v["lookstate"] == 2): ?>已读<?php else: ?> 未读<?php endif; ?></td>
					<td><?php echo (date('Y-m-d H:i:s',$v["addtime"])); ?></td>
					<td>

						<a href="<?php echo U('Feedback/edit',array('fd_id'=> $v['fd_id']));?>">
								查看详细
						</a>
						<span class="line">|</span>
						<a href="javascript:del_modal('<?php echo U('Feedback/del',array('fd_id'=> $v['fd_id']));?>')">
								删除
						</a>
					</td>
				</tr><?php endforeach; endif; ?>
			<tr>
					<td colspan="7" class='page'><?php echo ($page); ?></td>
				</tr>
			<?php else: ?>
			<tr>
				<td colspan="7">没有找到符合的记录</td>
			</tr><?php endif; ?>
	</table>
	<div class="h60"></div>
	<input type="button" class="hd-btn hd-btn-xm select_all"  value='全选'/>
	<input  type="button" class="hd-btn hd-btn-xm operation"  value="批量删除" name="update_del"/>
</form>

</body>
</html>