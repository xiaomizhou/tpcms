<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <title>欢迎信息</title>
    <script type='text/javascript' src='/dcms/Core/Org/Jquery/jquery-1.8.2.min.js'></script>
	<link href='/dcms/Core/Org/hdjs/hdjs.css' rel='stylesheet' media='screen'>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/hdjs.min.js'></script>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/org/cal/lhgcalendar.min.js'></script>
	<script type='text/javascript'>
		MODULE='/dcms/index.php/Admin'; //当前模块
		CONTROLLER='/dcms/index.php/Admin/Index'; //当前控制器)
		ACTION='/dcms/index.php/Admin/Index/copyright';//当前方法(方法)
		ROOT='/dcms'; //当前项目根路径
		PUBLIC= '/dcms/Core/Tpcms/Admin/View/Public';//当前定义的Public目录
	</script>
    <script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.base.js"></script>
    <link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.base.css" />
</head>
<body>
    <div class="hd-title-header">温馨提示</div>
    <table class="hd-table hd-table-list">
        <tbody>
            <tr>
                <td style="color:red;font-weight: bold;">欢迎使用TPCMS后台管理</td>
            </tr>
        </tbody>
    </table>
    <div class="hd-title-header">安全提示</div>
    <table class="hd-table hd-table-list">
        <tbody>
            <tr>
                <td>建议将hdcms目录(及子目录)设置为750,文件设置为640</td>
            </tr>
        </tbody>
    </table>
    <div style="height:10px;overflow: hidden">&nbsp;</div>
    <div class="hd-title-header">系统信息</div>
    <table class="hd-table hd-table-list">
        <tbody>
        	<?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if(($mod) == "0"): ?><tr>
	                <td style="width:25%;"><div><?php echo ($key); ?>：</div></td>
	                <td style="width:25%;"><div><?php echo ($vo); ?></div></td><?php endif; ?>
	            <?php if(($mod) == "1"): ?><td style="width:25%;"><div><?php echo ($key); ?>：</div></td>
	                <td style="width:25%;"><div><?php echo ($vo); ?></div></td>
	            </tr><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            <?php if($infonum == 1): ?><td style="width:25%;"></td>
                  <td style="width:25%;"></td>
              </tr><?php endif; ?>
        </tbody>
    </table>
    <div style="height:10px;overflow: hidden">&nbsp;</div>
    <div class="hd-title-header">程序团队</div>
    <table class="hd-table hd-table-list">
        <tbody>
            <tr>
                <td  class="hd-w100">项目负责人</td>
                <td>点击未来团队</td>
            </tr>
            <tr>
                <td>鸣谢</td>
                <td>
                    <a href="http://www.djie.net" target="_blank">点击未来</a>
                </td>
            </tr>
        </tbody>
    </table>
    <style type="text/css">
    a {
        color: #666;
    }

    div.wrap {
        margin-bottom: 0px;
    }

    h2 {
        font-size: 12px;
        font-weight: normal;
        margin-bottom: 10px;
    }

    div#RecordSite, div#RecordSite a {
        padding: 40px 0px 0px;
        font-size: 18px;
        text-align: center;
    }

    div#RecordSite a {
        color: #03565E;
        font-weight: bold;
        padding-left: 10px;
    }
</style>

</body>
</html>